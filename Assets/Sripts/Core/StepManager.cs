﻿using UnityEngine;

public class StepManager : MonoBehaviour
{
    public StepFileManager stepFileManager;
  
    public StepInfoDictionary dicStepInfo ;

    // Start is called before the first frame update
    private static StepManager instance;

    bool bIsLoading = false;
    public static StepManager Instance
    {
        get
        {
            if (instance == null)
            {
                var obj = FindObjectOfType<StepManager>();

                if (obj != null)
                {
                    instance = obj;
                }
                else
                {
                    var newSingleton = new GameObject("StepManager").AddComponent<StepManager>();
                    Instance = newSingleton;
                }
            }

            return instance;
        }

        private set
        {
            instance = value;
        }
    }

    private void Awake()
    {
        var objs = FindObjectsOfType<StepManager>();

        if (objs.Length != 1)
        {

            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);

    }
    public void Start()
    {
        stepFileManager.Load();
    }

    public void SaveStepFile()
    {
        stepFileManager.Save();
    }
    public void LoadStepFile()
    {
#if UNITY_EDITOR
#else
        if(bIsLoading == true)
        return; 
#endif
        stepFileManager.Load();
        bIsLoading = true;
    }
}
