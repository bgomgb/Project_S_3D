﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class GoogleAdsManager : MonoBehaviour
{
    public string android_banner_id;
    public string ios_banner_id;

    public string android_interstitial_id;
    public string ios_interstitial_id;

    private BannerView bannerView;
    private InterstitialAd interstitialAd;

    private static GoogleAdsManager instance;
    public static GoogleAdsManager Instance
    {
        get
        {
            if (instance == null)
            {
                var obj = FindObjectOfType<GoogleAdsManager>();

                if (obj != null)
                {
                    instance = obj;
                }
                else
                {
                    var newSingleton = new GameObject("GoogleAdsManager").AddComponent<GoogleAdsManager>();
                    instance = newSingleton;
                }
            }

            return instance;
        }

        private set
        {
            instance = value;
        }
    }

    private void Awake()
    {
        var objs = FindObjectsOfType<GoogleAdsManager>();

        if (objs.Length != 1)
        {

            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(this);
    }

    public void Start()
    {
        RequestBannerAd();
        RequestInterstitialAd();

        DontDestroyOnLoad(this);
    }

    public void RequestBannerAd()
    {
        string adUnitId = string.Empty;

#if UNITY_ANDROID
        adUnitId = android_banner_id;
#elif UNITY_IOS
        adUnitId = ios_bannerAdUnitId;
#endif

        bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);
        AdRequest request = new AdRequest.Builder().Build();

        bannerView.LoadAd(request);
        bannerView.Hide();
    }

    private void RequestInterstitialAd()
    {
        string adUnitId = string.Empty;

#if UNITY_ANDROID
        adUnitId = android_interstitial_id;
#elif UNITY_IOS
        adUnitId = ios_interstitialAdUnitId;
#endif

        interstitialAd = new InterstitialAd(adUnitId);
        AdRequest request = new AdRequest.Builder().Build();

        interstitialAd.LoadAd(request);

        interstitialAd.OnAdClosed += HandleOnInterstitialAdClosed;
    }

    public void HandleOnInterstitialAdClosed(object sender, EventArgs args)
    {
        print("HandleOnInterstitialAdClosed event received.");

        interstitialAd.Destroy();

        RequestInterstitialAd();
    }

    public void ShowBannerAd()
    {
        if (bannerView != null)
            bannerView.Show();
    }

    public void HideBannerAd()
    {
        if(bannerView != null)
            bannerView.Hide();
    }


    public void ShowInterstitialAd(bool isRandomBanner)
    {
        if (!interstitialAd.IsLoaded())
        {
            RequestInterstitialAd();
            return;
        }

        bool showAD = false;

        if (isRandomBanner)
        {

            if (PlayerManger.Instance.updateStageCount < 3)
                return;

            int value = UnityEngine.Random.Range(0, 100);

            switch (PlayerManger.Instance.updateStageCount)
            {
                case 3:
                    if(value < 25)
                    {
                        showAD = true;
                    }

                    break;
                case 4:
                    if (value < 50)
                    {
                        showAD = true;
                    }
                    break;
                case 5:
                    if (value < 75)
                    {
                        showAD = true;
                    }
                    break;
                case 6:
                    showAD = true;
                    break;

            }

        }

        if (showAD == false)
            return;

        interstitialAd.Show();

        PlayerManger.Instance.updateStageCount = 0;

    }
   
}

