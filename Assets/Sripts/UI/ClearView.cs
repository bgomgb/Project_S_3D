﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.Collections;

public class ClearView : MonoBehaviour
{
    public Button RestartButton;
    public Button NextButton;

    private void Start()
    {
        RestartButton.onClick.AddListener(() => Restart());
        NextButton.onClick.AddListener(()=>NextStage());        
    }
      

    public void NextStage()
    {
        StageManager.Instance.NextStage();
        gameObject.SetActive(false);
       GoogleAdsManager.Instance.ShowBannerAd();
        PlayerManger.Instance.fUpdateStageCount();
    }
    public void Restart()
    {
        StageManager.Instance.ReStart();
        gameObject.SetActive(false);
       GoogleAdsManager.Instance.ShowBannerAd();
        PlayerManger.Instance.fUpdateStageCount();
    }

}