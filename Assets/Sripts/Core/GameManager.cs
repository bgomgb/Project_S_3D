﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    public int PlayStage;

    public GameDefine.VictoryConditions victoryConditions;
    public int TurnLimit = 0,UseTurn = 0;
    public MapFileManager MapFileManager;

    public int ComboCount = 0, MaxComboCount = 0;

     // 필드 제한
    public int StageMaxComboCount = 0, StageEnemyCount = 0, StageDestinationCount = 0;

    // effect
    public GameObject boomeffect, comboEffect;

    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                var obj = FindObjectOfType<GameManager>();

                if (obj != null)
                {
                    instance = obj;
                }
                else
                {
                    var newSingleton = new GameObject("GameManager").AddComponent<GameManager>();
                    Instance = newSingleton;
                }
            }

            return instance;
        }

        private set
        {
            instance = value;
        }
    }

    private void Awake()
    {
        var objs = FindObjectsOfType<GameManager>();

        if (objs.Length != 1)
        {

            Destroy(gameObject);
            return;
        }
        //DontDestroyOnLoad(gameObject);

    }
   


    void Start()
    {
        StepManager.Instance.LoadStepFile();
        StageLoad(PlayerManger.Instance.PlayStage);     
    }

    public void PlayUseTurn()
    {
        TurnLimit--;
        UseTurn++;
    }
    public void StageLoad(int step)
    {
        StageEnemyCount       = 0;
        StageDestinationCount = 0;
        StageMaxComboCount    = 0;

        ComboCount            = 0;
        MaxComboCount         = 0;

        if (StepManager.Instance.dicStepInfo.ContainsKey(step) == false)
        {
            Debug.Log("Step Stage Error");
            return;
        }

        StageManager.Instance.stageName = StepManager.Instance.dicStepInfo[step].FileName;
        StageManager.Instance.listDestinationCube.Clear();
        StageManager.Instance.listMultyiDirectionCube.Clear();
        MapFileManager.Load(StageManager.Instance.stageName);

        StageManager.Instance.SpeicalUpdateCube();

        LoadCamera(step);  //Camera Load

        TurnLimit = StepManager.Instance.dicStepInfo[step].TurnLimit;
        victoryConditions = (GameDefine.VictoryConditions)StepManager.Instance.dicStepInfo[step].Victoryconditions;
        StageMaxComboCount = StepManager.Instance.dicStepInfo[step].ComboCount;
        UseTurn = 0;


        TopView.Instance.init();
    }

    void RayCastingTarget()
    {
        if (StageManager.Instance.IsMoveGem == true)
            return; 

        if (Input.GetMouseButtonDown(0))
         {
            IsDisplayPickUp = true;
         }

        if (IsDisplayPickUp)
        {
            RaycastHit hit;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            Physics.Raycast(ray, out hit);


            if (hit.collider != null)
            {
                if (hit.collider.tag == "VerTex")
                {
                    //Debug.Log($"체크 == {hit.collider.name}");

                    var parent = hit.collider.transform.parent;
                    var curCube = parent.GetComponent<Cube>();
                    //Debug.Log($"AA{curCube.mX.ToString() + "__" + curCube.mY.ToString()}");


                    StageManager.Instance.RotationDisplayCube(curCube, hit.collider.name);
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            IsDisplayPickUp = false;

            StartCoroutine(StageManager.Instance.WaitRotationCube());
        }

     
    }
    bool IsDisplayPickUp =false;
    // Update is called once per frame
    void Update()
    {
        RayCastingTarget();
    }

    public void SaveStage()
    {
        MapFileManager.Save();
    }
    public void LoadStage(string stageName)
    {
        MapFileManager.Load(stageName);
        
    }
    public void LoadCamera(int step)
    {
        Camera.main.transform.localPosition = new Vector3(StepManager.Instance.dicStepInfo[step].CameraInfoX,
                                                     StepManager.Instance.dicStepInfo[step].CameraInfoY,
                                                     StepManager.Instance.dicStepInfo[step].CameraInfoZ);

        //Camera.main.orthographicSize = StepManager.Instance.dicStepInfo[step].OthSize;
        Camera.main.fieldOfView = StepManager.Instance.dicStepInfo[step].OthSize;

        Camera.main.transform.localEulerAngles = new Vector3(StepManager.Instance.dicStepInfo[step].RotationX, StepManager.Instance.dicStepInfo[step].RotationY, StepManager.Instance.dicStepInfo[step].RotationZ);


        if(StageManager.Instance.HexagonField == null)
        {
            StageManager.Instance.HexagonField = GameObject.Find("Hexagon_Field");
        }

        StageManager.Instance.HexagonField.transform.localPosition = new Vector3(StepManager.Instance.dicStepInfo[step].HexagonFieldX,
                                                     StepManager.Instance.dicStepInfo[step].HexagonFieldY,
                                                     StepManager.Instance.dicStepInfo[step].HexagonFieldZ); ;
        StageManager.Instance.HexagonField.transform.localEulerAngles = new Vector3(StepManager.Instance.dicStepInfo[step].HexagonField_RotationX,
                                                     StepManager.Instance.dicStepInfo[step].HexagonField_RotationY,
                                                     StepManager.Instance.dicStepInfo[step].HexagonField_RotationZ); ;


    }

    public bool GameClearCheck()
    {

        switch(victoryConditions)
        {
            case GameDefine.VictoryConditions.All:
                if (StageEnemyCount == 0)
                {
                    if(MaxComboCount >= StageMaxComboCount)
                        return true;
                }
                break;

            case GameDefine.VictoryConditions.Destination:
                if (StageDestinationCount == 0)
                {
                    if (MaxComboCount >= StageMaxComboCount)
                        return true;
                }
                break;
            case GameDefine.VictoryConditions.ALL_DEST:
                if (StageEnemyCount == 0 && StageDestinationCount == 0)
                {
                    if (MaxComboCount >= StageMaxComboCount)
                        return true;
                }
                    
                break;
        }
        return false;
    }

    public void SaveStageClearinfo(bool bIsClear)
    {
            var playerStageClearInfo = PlayerManger.Instance.stageClearInfos.Find(x => x.Stage == PlayerManger.Instance.PlayStage);

        if (playerStageClearInfo == null)
            playerStageClearInfo = new StageClearInfo();

            playerStageClearInfo.bIsViewTurorial = true;
            playerStageClearInfo.StarRank = 0;
            playerStageClearInfo.Stage = PlayerManger.Instance.PlayStage;

            var stepinfo = GetCurrentStepInfo();
            if (bIsClear)
            {


                if (UseTurn <= stepinfo.Two_StarAttainmentTargetTurn)
                {

                    if (UseTurn <= stepinfo.Three_StarAttainmentTargetTurn)
                        playerStageClearInfo.StarRank = 3;
                    else
                        playerStageClearInfo.StarRank = 2;
                }
                else
                    playerStageClearInfo.StarRank = 1;
            }

             playerStageClearInfo.ClearTime = TopView.Instance.playtime;
             playerStageClearInfo.UseTurn = TurnLimit;


            if (bIsClear)
            {

                if (playerStageClearInfo.BestClearTime == 0)
                    playerStageClearInfo.BestClearTime = playerStageClearInfo.ClearTime;
                else
                    playerStageClearInfo.BestClearTime = Mathf.Min(playerStageClearInfo.BestClearTime, playerStageClearInfo.ClearTime);
            }
              playerStageClearInfo.BestClearRank = Mathf.Max(playerStageClearInfo.BestClearRank, playerStageClearInfo.StarRank);

        PlayerManger.Instance.stageClearInfos.Add(playerStageClearInfo);
        PlayerManger.Instance.Save();
    }

    public  StepInfo GetCurrentStepInfo()
    {
        return StepManager.Instance.dicStepInfo[PlayerManger.Instance.PlayStage];

    }


}
