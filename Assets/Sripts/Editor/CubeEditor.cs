﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

[CustomEditor(typeof(Cube))]
public class CubeEditor : Editor

{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Resize"))
        {
            Cube obj = target as Cube;

            obj.replace();
        }
    }
}