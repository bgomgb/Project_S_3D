﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(StepManager))]
public class StepFileButton : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Save"))
        {
            StepManager.Instance.SaveStepFile();
        }

        if (GUILayout.Button("Load"))
        {
            StepManager.Instance.LoadStepFile();
        }
    }
}

