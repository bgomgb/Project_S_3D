﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FailedView : MonoBehaviour
{
    public Button RestartButton,TitleButton;
    // Use this for initialization
    void Start()
    {
        RestartButton.onClick.AddListener(() => ReStart());
        TitleButton.onClick.AddListener(() => Title());
    }

    void ReStart()
    {
        StageManager.Instance.ReStart();
        gameObject.SetActive(false);
       GoogleAdsManager.Instance.ShowBannerAd();
        PlayerManger.Instance.fUpdateStageCount();
    }

    void Title()
    {
        SceneManager.LoadScene("Title");
        SoundManager.Instance.StopBgm();
       GoogleAdsManager.Instance.HideBannerAd();
    }
}
