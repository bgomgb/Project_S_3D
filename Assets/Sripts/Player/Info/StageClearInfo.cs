﻿using UnityEngine;
using UnityEditor;
using System;

[Serializable]
public class StageClearInfo
{
    [SerializeField]
    public int Stage;
    public int StarRank;
    public int BestClearRank;
    public float ClearTime;
    public float BestClearTime;

    public bool bIsViewTurorial;
    public int UseTurn;
    public int BestUseTurn;
    public StageClearInfo()
    {

    }
    public StageClearInfo(int _Stage,int _StarRank,int _BestClearRank, float _ClearTime,float _BestClearTime, bool _bIsViewTutorial, int _UseTurn,int _BestUseTurn)
    {
        Stage         = _Stage;
        StarRank      = _StarRank;
        BestClearRank = _BestClearRank;
        ClearTime     = _ClearTime;
        BestClearTime = _BestClearTime;
        bIsViewTurorial = _bIsViewTutorial;
        UseTurn         = _UseTurn;
        UseTurn = _BestUseTurn;
    }
}


public class ResultStageClearInfo
{
    public bool result;
    public StageClearInfo[] StageClearInfos;
}