﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class TitleScene : MonoBehaviour
{
    // Start is called before the first frame update
    public Button btnContinue,btnStageSelect,btnExit;
    public GameObject SelectView;
    void Start()
    {
        Screen.SetResolution(1080, 1920, true);
        btnContinue.onClick.AddListener(() => ContinueGame());
        btnStageSelect.onClick.AddListener(() => StageSelect());
        btnExit.onClick.AddListener(() => ExitGame());
    }   

    public void ContinueGame()
    {
        LoadScene();
    }

    public IEnumerator MainSceneLoad()
    {
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync("Main");

        while (!asyncOperation.isDone)
        {
            //if(asyncOperation.progress>= 1.0f)
            //    GameManager.Instance.StageLoad(PlayerManger.Instance.PlayStage);

            SoundManager.Instance.playBgm("InGameBGM_Bubble");
           GoogleAdsManager.Instance.ShowBannerAd();

            yield return null;
        }
    }

    public void LoadScene()
    {
        StartCoroutine(MainSceneLoad());
    }

    public void StageSelect()
    {
        SelectView.GetComponent<StageSelect>().StageSelectOn();
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
