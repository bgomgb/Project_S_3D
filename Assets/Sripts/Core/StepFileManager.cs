﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class StepFileManager : MonoBehaviour
{
    private string strFileName =  "Csv/StepMap";
    public void Save()
    {
        

        if (StepManager.Instance.dicStepInfo.ContainsKey(StageManager.Instance.step) == false)
        {
            Debug.Log("ADD new Step Info");

            StepInfo stepinfo = new StepInfo();
            stepinfo.Step     = StageManager.Instance.step;
            stepinfo.FileName = StageManager.Instance.stageName;

            stepinfo.CameraInfoX = Camera.main.transform.localPosition.x;
            stepinfo.CameraInfoY = Camera.main.transform.localPosition.y;
            stepinfo.CameraInfoZ = Camera.main.transform.localPosition.z;

            stepinfo.RotationX = Camera.main.transform.eulerAngles.x;
            stepinfo.RotationY = Camera.main.transform.eulerAngles.y;
            stepinfo.RotationZ = Camera.main.transform.eulerAngles.z;

            stepinfo.HexagonFieldX = StageManager.Instance.HexagonField.transform.localPosition.x;
            stepinfo.HexagonFieldY = StageManager.Instance.HexagonField.transform.localPosition.y;
            stepinfo.HexagonFieldZ = StageManager.Instance.HexagonField.transform.localPosition.z;

            stepinfo.HexagonField_RotationX = StageManager.Instance.HexagonField.transform.eulerAngles.x;
            stepinfo.HexagonField_RotationY = StageManager.Instance.HexagonField.transform.eulerAngles.y;
            stepinfo.HexagonField_RotationZ = StageManager.Instance.HexagonField.transform.eulerAngles.z;

            //Test Version
            //stepinfo.OthSize = Camera.main.orthographicSize;
            stepinfo.OthSize     = Camera.main.fieldOfView;

            StepManager.Instance.dicStepInfo.Add(stepinfo.Step, stepinfo);
        }
        else
        {
            StepManager.Instance.dicStepInfo[StageManager.Instance.step].Step        = StageManager.Instance.step;
            StepManager.Instance.dicStepInfo[StageManager.Instance.step].FileName    = StageManager.Instance.stageName;
            StepManager.Instance.dicStepInfo[StageManager.Instance.step].CameraInfoX = Camera.main.transform.localPosition.x;
            StepManager.Instance.dicStepInfo[StageManager.Instance.step].CameraInfoY = Camera.main.transform.localPosition.y;
            StepManager.Instance.dicStepInfo[StageManager.Instance.step].CameraInfoZ = Camera.main.transform.localPosition.z;
            
            //StepManager.Instance.dicStepInfo[StageManager.Instance.step].OthSize = Camera.main.orthographicSize;
            StepManager.Instance.dicStepInfo[StageManager.Instance.step].OthSize = Camera.main.fieldOfView;


            StepManager.Instance.dicStepInfo[StageManager.Instance.step].RotationX = (float) Camera.main.transform.eulerAngles.x;
            StepManager.Instance.dicStepInfo[StageManager.Instance.step].RotationY = (float)Camera.main.transform.eulerAngles.y;
            StepManager.Instance.dicStepInfo[StageManager.Instance.step].RotationZ = (float)Camera.main.transform.eulerAngles.z;

            StepManager.Instance.dicStepInfo[StageManager.Instance.step].HexagonFieldX = StageManager.Instance.HexagonField.transform.localPosition.x;
            StepManager.Instance.dicStepInfo[StageManager.Instance.step].HexagonFieldY = StageManager.Instance.HexagonField.transform.localPosition.y;
            StepManager.Instance.dicStepInfo[StageManager.Instance.step].HexagonFieldZ = StageManager.Instance.HexagonField.transform.localPosition.z;

            StepManager.Instance.dicStepInfo[StageManager.Instance.step].HexagonField_RotationX = StageManager.Instance.HexagonField.transform.eulerAngles.x;
            StepManager.Instance.dicStepInfo[StageManager.Instance.step].HexagonField_RotationY = StageManager.Instance.HexagonField.transform.eulerAngles.y;
            StepManager.Instance.dicStepInfo[StageManager.Instance.step].HexagonField_RotationZ = StageManager.Instance.HexagonField.transform.eulerAngles.z;

        }

        
        using (var writer = new CsvFileWriter($"Assets/Resources/{strFileName}.csv"))
        {
            Debug.Log("Save");

            List<string> columns = new List<string>() { "Step", "FileName", "CameraX", "CameraY", "CameraZ", 
                                                        "Size","RotationX","RotationY","RotationZ","Victoryconditions","TurnLimit","ComboCount"
                                                        ,"Two_StarAttainmentTargetTurn","Three_StarAttainmentTargetTurn"
                                                        , "HexagonFieldX","HexagonFieldY","HexagonFieldZ"
                                                        , "HexagonField_RotationX","HexagonField_RotationY","HexagonField_RotationZ"};// making Index Row

            writer.WriteRow(columns);
            columns.Clear();

            var orderdby = StepManager.Instance.dicStepInfo.OrderBy(num => num.Key);

            foreach (var dic  in orderdby)
            {
                columns.Add(dic.Value.Step.ToString());
                columns.Add(dic.Value.FileName);
                columns.Add(dic.Value.CameraInfoX.ToString());
                columns.Add(dic.Value.CameraInfoY.ToString());
                columns.Add(dic.Value.CameraInfoZ.ToString());
                columns.Add(dic.Value.OthSize.ToString());

                columns.Add(dic.Value.RotationX.ToString());
                columns.Add(dic.Value.RotationY.ToString());
                columns.Add(dic.Value.RotationZ.ToString());

                columns.Add(dic.Value.Victoryconditions.ToString());
                columns.Add(dic.Value.TurnLimit.ToString());
                columns.Add(dic.Value.ComboCount.ToString());

                columns.Add(dic.Value.Two_StarAttainmentTargetTurn.ToString());
                columns.Add(dic.Value.Three_StarAttainmentTargetTurn.ToString());


                columns.Add(dic.Value.HexagonFieldX.ToString());
                columns.Add(dic.Value.HexagonFieldY.ToString());
                columns.Add(dic.Value.HexagonFieldZ.ToString());

                columns.Add(dic.Value.HexagonField_RotationX.ToString());
                columns.Add(dic.Value.HexagonField_RotationY.ToString());
                columns.Add(dic.Value.HexagonField_RotationZ.ToString());

                writer.WriteRow(columns);
                columns.Clear();
            }
        }
    }

    public void Load()
    {

        List<Dictionary<string, object>> data = CsvReader.Read(strFileName);

        if (StepManager.Instance.dicStepInfo.Count != 0)
        {
            StepManager.Instance.dicStepInfo.Clear();
        }

        for (var i = 0; i< data.Count; i++)
        {

                StepInfo readObj = new StepInfo();

                readObj.Step        = int.Parse(data[i]["Step"].ToString());
                readObj.FileName    =            (data[i]["FileName"].ToString());
                readObj.CameraInfoX = float.Parse(data[i]["CameraX"].ToString());
                readObj.CameraInfoY = float.Parse(data[i]["CameraY"].ToString());
                readObj.CameraInfoZ = float.Parse(data[i]["CameraZ"].ToString());
                readObj.OthSize     = float.Parse(data[i]["Size"].ToString());

                readObj.RotationX   = float.Parse(data[i]["RotationX"].ToString());
                readObj.RotationY   = float.Parse(data[i]["RotationY"].ToString());
                readObj.RotationZ   = float.Parse(data[i]["RotationZ"].ToString());

                readObj.Victoryconditions  = int.Parse(data[i]["Victoryconditions"].ToString());
                readObj.TurnLimit          = int.Parse(data[i]["TurnLimit"].ToString());
                readObj.ComboCount         = int.Parse(data[i]["ComboCount"].ToString());

                readObj.Two_StarAttainmentTargetTurn = int.Parse(data[i]["Two_StarAttainmentTargetTurn"].ToString());
                readObj.Three_StarAttainmentTargetTurn = int.Parse(data[i]["Three_StarAttainmentTargetTurn"].ToString());

                readObj.HexagonFieldX = float.Parse(data[i]["HexagonFieldX"].ToString());
                readObj.HexagonFieldY = float.Parse(data[i]["HexagonFieldY"].ToString());
                readObj.HexagonFieldZ = float.Parse(data[i]["HexagonFieldZ"].ToString());

                readObj.HexagonField_RotationX = float.Parse(data[i]["HexagonField_RotationX"].ToString());
                readObj.HexagonField_RotationY = float.Parse(data[i]["HexagonField_RotationY"].ToString());
                readObj.HexagonField_RotationZ = float.Parse(data[i]["HexagonField_RotationZ"].ToString());



            StepManager.Instance.dicStepInfo.Add(readObj.Step, readObj);
        }
    }
}
