﻿using System.Collections.Generic;
using UnityEngine;


public class PlayerManger : MonoBehaviour
{
    private static PlayerManger instance;
    public int PlayStage = 1;
    public int ClearStage = 1;
    public bool bIsTutorial = true;
    public int EndGameStage = 70;

    [SerializeField]
    public List<StageClearInfo> stageClearInfos = new List<StageClearInfo>();

    public bool isSuperAccount = false;

    public int updateStageCount;

    public static PlayerManger Instance
    {
        get
        {
            if (instance == null)
            {
                var obj = FindObjectOfType<PlayerManger>();

                if (obj != null)
                {
                    instance = obj;
                }
                else
                {
                    var newSingleton = new GameObject("PlayerManger").AddComponent<PlayerManger>();
                    Instance = newSingleton;
                }
            }

            return instance;
        }

        private set
        {
            instance = value;
        }
    }
    public void SavePlayStage()
    {
        PlayerPrefs.SetInt("PlayStage", PlayStage);
        PlayerPrefs.SetInt("ClearStage", ClearStage);
    }

    public void LoadPlayStage()
    {
        if(PlayerPrefs.HasKey("PlayStage"))
        {
            PlayStage = PlayerPrefs.GetInt("PlayStage");
            ClearStage = PlayerPrefs.GetInt("ClearStage");
        }
    }


    public void fUpdateStageCount()      //Call Ads
    {

        if (isSuperAccount)
            return;

        updateStageCount++;


        if(updateStageCount == 3)
        {
            GoogleAdsManager.Instance.ShowInterstitialAd(true);
            updateStageCount = 0;
        }

    }
    private void Awake()
    {
        var objs = FindObjectsOfType<PlayerManger>();

        if (objs.Length != 1)
        {

            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        Load();
    }
    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            Save();                  
        }
    }

    public void Save()
    {
        Debug.Log("저장");

        SavePlayStage();
        JsonManager.Instance.PlayerJsonSave();
        

    }

    public void Load()
    {
        Debug.Log("불러오기");

        LoadPlayStage();
        JsonManager.Instance.PlayerJsonLoad();
    }


}