﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoView : MonoBehaviour
{
    public Button close;
    public VideoPlayer videoPlayer;
    public VideoClip tutorialvideo;
    public GameObject viewobj;
    public RenderTexture viewRender;

    // Start is called before the first frame update
    
    void Start()
    {
        videoPlayer = viewobj.gameObject.transform.GetComponentInChildren<VideoPlayer>();
        close.onClick.AddListener(() => Close());
    }

    public void Init()
    {
        GoogleAdsManager.Instance.HideBannerAd();

        tutorialvideo = Resources.Load<VideoClip>($"Video\\{PlayerManger.Instance.PlayStage.ToString()}");

        if(tutorialvideo!=null)
        {
            TopView.Instance.isPlayTime = false;
            viewobj.SetActive(true);
            StartCoroutine(PlayVideo());
        }
    }

    IEnumerator PlayVideo()
    {
        videoPlayer.clip = tutorialvideo; 
        videoPlayer.isLooping = false;
        videoPlayer.Play();
        yield return null;
    }

    public void Close()
    {
        viewobj.SetActive(false);
        PlayerManger.Instance.bIsTutorial = false;
        TopView.Instance.isPlayTime = true;
        
        GoogleAdsManager.Instance.ShowBannerAd();
        videoPlayer.Stop();
        videoPlayer.clip = null;
        
        viewRender.Release();

        tutorialvideo = null; 
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
