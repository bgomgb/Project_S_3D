﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System;

[Serializable]
public class Serialization<T>
{
    [SerializeField]
    List<T> target;
    public List<T> ToList() { return target; }

    public Serialization(List<T> target)
    {
        this.target = target;
    }
}

public class JsonManager : MonoBehaviour
{
    private static JsonManager instance;
    public static JsonManager Instance
    {
        get
        {
            if (instance == null)
            {
                var obj = FindObjectOfType<JsonManager>();

                if (obj != null)
                {
                    instance = obj;
                }
                else
                {
                    var newSingleton = new GameObject("JsonManager").AddComponent<JsonManager>();
                    Instance = newSingleton;
                }
            }

            return instance;
        }

        private set
        {
            instance = value;
        }
    }
    private void Awake()
    {
        var objs = FindObjectsOfType<JsonManager>();

        if (objs.Length != 1)
        {

            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);

    }


    public void PlayerJsonSave()
    {        
        string data= JsonUtility.ToJson(new Serialization<StageClearInfo>(PlayerManger.Instance.stageClearInfos));
        Debug.Log("ToJson : " + data.ToString());


#if UNITY_EDITOR
        File.WriteAllText(Application.dataPath  + "/Resources/Json/StageClearInfo.json", data.ToString());
#else
        File.WriteAllText(Application.persistentDataPath + "StageClearInfo.json", data.ToString());
#endif
        
    }

    public void PlayerJsonLoad()
    {
        if(PlayerManger.Instance.stageClearInfos != null)
            PlayerManger.Instance.stageClearInfos.Clear();

        Debug.Log("불러오기");
#if UNITY_EDITOR
        if (File.Exists(Application.dataPath + "/Resources/Json/StageClearInfo.json") == false)
#else
        if (File.Exists(Application.persistentDataPath + "StageClearInfo.json") == false)
#endif
        {
            Debug.Log("불러오기 실패");
            return;
        }


#if UNITY_EDITOR
        string Jsonstring = File.ReadAllText(Application.dataPath + "/Resources/Json/StageClearInfo.json");
#else
         string Jsonstring = File.ReadAllText(Application.persistentDataPath + "StageClearInfo.json");
#endif

        if (Jsonstring.Length == 0)
            return;


        Serialization<StageClearInfo> reslt = JsonUtility.FromJson<Serialization<StageClearInfo>> (Jsonstring);

        PlayerManger.Instance.stageClearInfos = reslt.ToList();
    }
}
