﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(StageManager))]
public class CubeGenerateButton : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Generate Cubes"))
        {
            StageManager.Instance.CreateStage();
        }

        if (GUILayout.Button("Save Map"))
        {
            GameManager.Instance.SaveStage();
        }

        if (GUILayout.Button("Load Map"))
        {
            GameManager.Instance.LoadStage(StageManager.Instance.stageName);
        }

        if (GUILayout.Button("Load Camera"))
        {
            GameManager.Instance.LoadCamera(StageManager.Instance.step);
        }
    }
}

