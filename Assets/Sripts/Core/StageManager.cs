﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class StageManager : MonoBehaviour
{
    public GameObject[,] arrayMap;

    public int mWidth, mHeight;
    private GameObject cube;

    public bool IsMoveGem = false;
    public int step;
    public string stageName;

    public GameObject DefaultHexagon;
    private static StageManager instance;
    public GameObject HexagonField;


    public static StageManager Instance
    {
        get
        {
            if (instance == null)
            {
                var obj = FindObjectOfType<StageManager>();

                if (obj != null)
                {
                    instance = obj;
                }
                else
                {
                    var newSingleton = new GameObject("Stage Manager").AddComponent<StageManager>();
                    instance = newSingleton;
                }
            }

            return instance;
        }

        private set
        {
            instance = value;
        }
    }

    private void Awake()
    {
        var objs = FindObjectsOfType<StageManager>();

        if (objs.Length != 1)
        {

            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);

    }



    public void CreateStage()
    {
        cube = GameObject.Find("Hexagon_Field");

        if (cube.transform.childCount != 0)
        {
            for (int i = cube.transform.childCount - 1; i >= 0; i--)
                DestroyImmediate(cube.transform.GetChild(i).gameObject);
        }


        arrayMap = new GameObject[mWidth, mHeight];

        if (cube == null)
        {
            Debug.Log("Gems 오브젝트를 찾을수 없다");
            return;
        }

        //1920_1080
        Debug.Log($"{Screen.width} _ {Screen.height}");

        float top = cube.transform.position.y;

        float gap = 0.25f;
        //int iy = (int)(1080 - (mWidth - 1) * gap);
             float blockWidth = 5.9f;
        float blockHight = 5.15f;

        float fixX = 0;// blockWidth/2;
        float fixY = 0;// blockHight/2;


        for (var i = 0; i < mWidth; i++)
        {
            if (i != 0)
                fixX += blockWidth / 2 + (blockWidth / 4) + gap;
            if (i % 2 == 0)
                fixY = blockHight;
            else
                fixY = blockHight / 2;
            for (var j = 0; j < mHeight; j++)
            {
                CreateObjcet(i, j, fixX, fixY + ((blockHight + gap) * j), blockWidth, blockHight);
            }
        }

        CheckVertexBox();
        IsMoveGem = false;
    }

    List<GameObject> VertexBoxList = new List<GameObject>();
    public void CheckVertexBox()
    {
        //0,1,2,3,4,5,6
        string[] arrayDir = { "L_T", "L_C", "L_B", "R_T", "R_C", "R_B" };
        List<GameObject> listDir = new List<GameObject>();

        for (int x = 0; x < mWidth; x++)
        {
            for (int y = 0; y < mHeight; y++)
            {
                Cube cube = arrayMap[x, y].GetComponent<Cube>();
                listDir.Clear();
                foreach (var dir in arrayDir)
                {
                    var obj = cube.transform.Find(dir);
                    obj.gameObject.SetActive(false);
                    listDir.Add(obj.gameObject);
                }

                {
                    //Onepoint
                    {
                        Cube R = getArrayDirMapCube(cube.mX, cube.mY, GameDefine.Direction.Right);
                        Cube RD = getArrayDirMapCube(cube.mX, cube.mY, GameDefine.Direction.Right_Down);
                        if(R != null && RD !=null)
                        {
                            if (R.mCode != GameDefine.CubeProperty.None && RD.mCode != GameDefine.CubeProperty.None)
                            {
                                if(cube.mCode!= GameDefine.CubeProperty.Normal||  R.mCode != GameDefine.CubeProperty.Normal || RD.mCode != GameDefine.CubeProperty.Normal)
                                {
                                    if(!(cube.mCode == R.mCode && R.mCode == RD.mCode))
                                    {
                                        var dir = listDir.Find(find => find.name == "R_C");

                                        if (dir != null)
                                        {
                                            dir.gameObject.SetActive(true);
                                            VertexBoxList.Add(dir);
                                        }
                                            
                                    }
                                  
                                }


                            }
                        }
                        
                    }

                    //Twopoint
                    {
                       
                        Cube RD = getArrayDirMapCube(cube.mX, cube.mY, GameDefine.Direction.Right_Down);
                        Cube D = getArrayDirMapCube(cube.mX, cube.mY, GameDefine.Direction.Down);
                        if (D != null && RD != null)
                        {
                            if (D.mCode != GameDefine.CubeProperty.None && RD.mCode != GameDefine.CubeProperty.None)
                            {
                                if (cube.mCode != GameDefine.CubeProperty.Normal || D.mCode != GameDefine.CubeProperty.Normal || RD.mCode != GameDefine.CubeProperty.Normal)
                                {
                                    if(!(cube.mCode ==  D.mCode && D.mCode== RD.mCode))
                                    {
                                        var dir = listDir.Find(find => find.name == "R_B");

                                        if (dir != null)
                                        {
                                            dir.gameObject.SetActive(true);
                                            VertexBoxList.Add(dir);
                                        }
                                            
                                    }

                                    
                                }
                                    
                            }
                        }

                    }
                }
            }
        }
    }

    public Cube getArrayDirMapCube(int X, int Y, GameDefine.Direction dir)
    {
        bool isEven = X % 2 == 0;
        GameObject cube = null;

        switch (dir)
        {
            case GameDefine.Direction.UP:
                cube = getPosCube(new Vector2(X, Y - 1));
                break;

            case GameDefine.Direction.Down:
                cube = getPosCube(new Vector2(X, Y + 1));
                break;

            case GameDefine.Direction.Left:
                if (isEven)
                    cube = getPosCube(new Vector2(X - 1, Y));
                else
                    cube = getPosCube(new Vector2(X - 1, Y - 1));
                break;

            case GameDefine.Direction.Right:
                if (isEven)
                    cube = getPosCube(new Vector2(X + 1, Y));
                else
                    cube = getPosCube(new Vector2(X + 1, Y - 1));
                break;

            case GameDefine.Direction.Right_Down:
                if (isEven)
                    cube = getPosCube(new Vector2(X + 1, Y + 1));
                else
                    cube = getPosCube(new Vector2(X + 1, Y));
                break;

            case GameDefine.Direction.Left_Down:
                if (isEven)
                    cube = getPosCube(new Vector2(X - 1, Y + 1));
                else
                    cube = getPosCube(new Vector2(X - 1, Y));
                break;

        }

        if (cube == null)
            return null;

        return cube.GetComponent<Cube>();
    }




    public void CreateObjcet(int X, int Y, float fixX, float fixz, float blockWidth, float blockHeight, GameDefine.CubeProperty code = GameDefine.CubeProperty.Normal)
    {
        //GameDefine.GemGode objectCode = objectCode = GameDefine.GemGode.None;
        string colorcode = "FFFFFF";
        cube = GameObject.Find("Hexagon_Field");

        arrayMap[X, Y] = Instantiate(DefaultHexagon);
        arrayMap[X, Y].transform.parent = cube.transform;

        arrayMap[X, Y].name = ($"{X}_{Y}");

        colorcode = GameDefine.getColor(code);
        {
            var obj = arrayMap[X, Y].AddComponent<Cube>();

            //arrayMap[X, Y].AddComponent<Drag>();

            obj.mCode = code;
            obj.mX = X;
            obj.mY = Y;

            arrayMap[X, Y].transform.localPosition = new Vector3(-fixX, 0, fixz);
            arrayMap[X, Y].transform.localScale = new Vector3(3f, 0.1f, 3f);

            obj.replace();

        }
    }

    public void CreateObjcet(int X, int Y, float fixX, float fixz, float blockWidth, float blockHeight, Cube readCube)
    {
        //GameDefine.GemGode objectCode = objectCode = GameDefine.GemGode.None;
        string colorcode = "FFFFFF";
        cube = GameObject.Find("Hexagon_Field");

        arrayMap[X, Y] = Instantiate(DefaultHexagon);
        arrayMap[X, Y].transform.parent = cube.transform;

        arrayMap[X, Y].name = ($"{X}_{Y}");


        colorcode = GameDefine.getColor(readCube.mCode);
        {
            var obj = arrayMap[X, Y].AddComponent<Cube>();
            //arrayMap[X, Y].AddComponent<Drag>();

            obj.mCode = readCube.mCode;
            obj.mX = X;
            obj.mY = Y;

            obj.Destination = readCube.Destination;
            obj.Screens = readCube.Screens;
            obj.MultidirectionalRecognitionCount = readCube.MultidirectionalRecognitionCount;
            obj.InfectionmovingDir = readCube.InfectionmovingDir;
            obj.bIsFixedBlock = readCube.bIsFixedBlock;

            if (obj.Destination == true)
            {
                GameManager.Instance.StageDestinationCount++;
                StageManager.Instance.listDestinationCube.Add(obj);
            }
            if(obj.mCode == GameDefine.CubeProperty.MultidirectionalRecognition)
            {
                StageManager.instance.listMultyiDirectionCube.Add(obj);
            }

            obj.MDRCounttextMesh = obj.gameObject.transform.GetComponentInChildren<TextMesh>();

            arrayMap[X, Y].transform.localPosition = new Vector3(-fixX, 0, fixz);
            arrayMap[X, Y].transform.localScale = new Vector3(3f, 0.1f, 3f);
            //arrayMap[X, Y].transform.localPosition += new Vector3(fixX, fixY,0);

            obj.replace();

            //arrayMap[X, Y].AddComponent<Drag>();
            // sprRender.sortingOrder = 100;
            // boxColider.size = new Vector2(134.0f, 134.0f);

        }
    }
    public GameObject getPosCube(Vector2 pos)
    {
        if (pos.x < 0 || pos.x >= mWidth || pos.y < 0 || pos.y >= mHeight)
            return null;

        return arrayMap[(int)pos.x, (int)pos.y];
    }


    //Cube List 관련 
    List<Cube>  listCube                = new List<Cube>();                 //
    List<Cube>  listExplosionCube       = new List<Cube>();
    List<Cube>  listInfectionMoveCube   = new List<Cube>();

    Stack<Cube> stackmovingCube          = new Stack<Cube>();

    public List<Cube> listMultyiDirectionCube = new List<Cube>();


    public string disDir;
    public Cube displayCube = null;
    public List<Cube> displayCubes = new List<Cube>();
    public List<Cube> listDestinationCube = new List<Cube>();
    public void releaseDisplayCube()
    {
        foreach (var dp in displayCubes)
        {
            arrayMap[dp.mX, dp.mY].SetActive(true);
            
            Destroy(dp.gameObject);
        }
        

        displayCubes.Clear();
    }


    string setPickDir;
    public void RotationDisplayCube(Cube curCube, string Dir)
    {
        Cube leftCube = null, rightCube = null;

        bool IsEven = curCube.mX % 2 == 0;

        if (displayCube != null && (displayCube.mX == curCube.mX && displayCube.mY == curCube.mY))
            return;

        releaseDisplayCube();

        switch (Dir)
        {
            case "L_T":
                {
                    //  (T/L)
                    leftCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.UP);
                    rightCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Left);

                    break;
                }

            case "L_C":
                {
                    // (L / LD)
                    leftCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Left);
                    rightCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Left_Down);

                    break;
                }

            case "L_B":
                {
                    // (B / L)
                    leftCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Left_Down);
                    rightCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Down);

                    break;
                }

            case "R_T":
                {
                    //(R / T)
                    leftCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Right);
                    rightCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.UP);

                    break;
                }

            case "R_C":
                {
                    //(R/RD)
                    leftCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Right_Down);
                    rightCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Right);

                    break;
                }

            case "R_B":
                {
                    //(RD / B)
                    leftCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Down);
                    rightCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Right_Down);

                    break;
                }
        }

        if (leftCube == null || rightCube == null)
            return;

        //if (curCube.mCode == GameDefine.CubeProperty.Normal && leftCube.mCode == GameDefine.CubeProperty.Normal && rightCube.mCode == GameDefine.CubeProperty.Normal)
        //    return;

        if (leftCube.mCode == GameDefine.CubeProperty.None || curCube.mCode == GameDefine.CubeProperty.None || rightCube.mCode == GameDefine.CubeProperty.None)
            return;

        SoundManager.Instance.playSound("UP");

        //SetDisplay Cube
        {

            if(HexagonField == null)
            {
                HexagonField = GameObject.Find("Hexagon_Field");
            }

            Cube icCueb = Instantiate(curCube,HexagonField.transform);
            displayCubes.Add(icCueb);
            Cube ilCueb = Instantiate(leftCube, HexagonField.transform);
            displayCubes.Add(ilCueb);
            Cube irCueb = Instantiate(rightCube, HexagonField.transform);
            displayCubes.Add(irCueb);

            curCube.gameObject.SetActive(false);
            leftCube.gameObject.SetActive(false);
            rightCube.gameObject.SetActive(false);

            icCueb.transform.localPosition = new Vector3(icCueb.transform.localPosition.x, 2f, icCueb.transform.localPosition.z);
            ilCueb.transform.localPosition = new Vector3(ilCueb.transform.localPosition.x, 2f, ilCueb.transform.localPosition.z);
            irCueb.transform.localPosition = new Vector3(irCueb.transform.localPosition.x, 2f, irCueb.transform.localPosition.z);

            icCueb.transform.eulerAngles = HexagonField.transform.eulerAngles;
            ilCueb.transform.eulerAngles = HexagonField.transform.eulerAngles;
            irCueb.transform.eulerAngles = HexagonField.transform.eulerAngles;



            // 추후 프리팹으로 수정 
            for (int i = 0; i < icCueb.transform.childCount; i++)
            {
                icCueb.transform.GetChild(i).gameObject.SetActive(false);
            }

            for (int i = 0; i < ilCueb.transform.childCount; i++)
            {
                ilCueb.transform.GetChild(i).gameObject.SetActive(false);
            }

            for (int i = 0; i < irCueb.transform.childCount; i++)
            {
                irCueb.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
        setPickDir = Dir;
    }



    //체크 (1)
    int CurrentComboCount;
    public void RotationCube(Cube curCube, string Dir)
    {
        Cube leftCube = null, rightCube = null;

        bool IsEven = curCube.mX % 2 == 0;

        switch (Dir)
        {
            case "L_T":
                {
                    //  (T/L)
                    leftCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.UP);
                    rightCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Left);

                    break;
                }

            case "L_C":
                {
                    // (L / LD)
                    leftCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Left);
                    rightCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Left_Down);

                    break;
                }

            case "L_B":
                {
                    // (B / L)
                    leftCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Left_Down);
                    rightCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Down);

                    break;
                }


            case "R_T":
                {
                    //(R / T)
                    leftCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Right);
                    rightCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.UP);

                    break;
                }

            case "R_C":
                {
                    //(R/RD)
                    leftCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Right_Down);
                    rightCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Right);

                    break;
                }

            case "R_B":
                {
                    //(RD / B)
                    leftCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Down);
                    rightCube = getArrayDirMapCube(curCube.mX, curCube.mY, GameDefine.Direction.Right_Down);

                    break;
                }
        }

        CurrentComboCount = GameManager.Instance.ComboCount;

        if (leftCube == null || rightCube == null || leftCube.mCode == GameDefine.CubeProperty.None || rightCube.mCode == GameDefine.CubeProperty.None || curCube.mCode == GameDefine.CubeProperty.None)
            return;

        //안정잉 요청사항
        if (curCube.bIsFixedBlock ||  leftCube.bIsFixedBlock || rightCube.bIsFixedBlock)
            return;

        foreach(var data in VertexBoxList)
        {
            if(data !=null)
                data.gameObject.SetActive(false);
        }
        VertexBoxList.Clear();

        GameManager.Instance.PlayUseTurn();
        TopView.Instance.UpdateText();       

        IsMoveGem = true;


        GameObject tempObject = new GameObject();
        Cube tempCube = tempObject.AddComponent<Cube>();

        tempCube.RotationCopy(curCube);

        curCube.RotationCopy(leftCube);
        leftCube.RotationCopy(rightCube);
        rightCube.RotationCopy(tempCube);

        curCube.replace();
        leftCube.replace();
        rightCube.replace();

        expansionCube(curCube);
        expansionCube(leftCube);
        expansionCube(rightCube);

        StartCoroutine(WaitForCubeFunction());
        Destroy(tempObject);
        
    }
    public IEnumerator WaitlockCube()
    {
        IsMoveGem = true;
        float setTime = 0.2f;
        SoundManager.Instance.playSound("Lock");
        GameObject CurCube = arrayMap[displayCubes[0].mX, displayCubes[0].mY];

        Vector3 posCur, posLeft, postRight;
        posCur = displayCubes[0].transform.position;
        posLeft = displayCubes[1].transform.position;
        postRight = displayCubes[2].transform.position;

        displayCubes[0].transform.DOPunchPosition(postRight.normalized, setTime);//.SetOptions(true).SetEase(Ease.Linear);     
        displayCubes[1].transform.DOPunchPosition(posCur.normalized, setTime);//.SetOptions(true).SetEase(Ease.Linear);
        displayCubes[2].transform.DOPunchPosition(posLeft.normalized, setTime);//.SetOptions(true).SetEase(Ease.Linear);
        
        yield return new WaitForSeconds(setTime);
        StageManager.Instance.releaseDisplayCube();
        IsMoveGem = false;
        
    }
    public IEnumerator WaitRotationCube()
    {
        // cur left right
        if(displayCubes.Count ==0)
            yield  break;

        if(displayCubes[0].bIsFixedBlock || displayCubes[1].bIsFixedBlock || displayCubes[2].bIsFixedBlock)
        {
            StartCoroutine(WaitlockCube());
            yield break;
        }

        IsMoveGem = true;
        GameObject CurCube = arrayMap[displayCubes[0].mX, displayCubes[0].mY];

        Vector3 posCur,posLeft,postRight;
        //겹칠때 화면이 깨져보이는 문제때문에 높이값수정
        displayCubes[0].transform.position = new Vector3(displayCubes[0].transform.position.x, 10.0f, displayCubes[0].transform.position.z);
        displayCubes[1].transform.position = new Vector3(displayCubes[1].transform.position.x, 9.0f, displayCubes[1].transform.position.z); ;
        displayCubes[2].transform.position = new Vector3(displayCubes[2].transform.position.x, 11.0f, displayCubes[2].transform.position.z); ;

        posCur      = displayCubes[0].transform.position;
        posLeft     = displayCubes[1].transform.position;
        postRight   = displayCubes[2].transform.position;

        //displayCubes[1].transform.position = new Vector3(transform.position.x, transform.position.y-0.1f, transform.position.z);
        //displayCubes[2].transform.position = new Vector3(transform.position.x, transform.position.y+1.0f, transform.position.z);

        float setTime = 0.3f;
        displayCubes[0].transform.DOMove(postRight, setTime);//.SetOptions(true).SetEase(Ease.Linear);     
        displayCubes[1].transform.DOMove(posCur, setTime);//.SetOptions(true).SetEase(Ease.Linear);
        displayCubes[2].transform.DOMove(posLeft, setTime);//.SetOptions(true).SetEase(Ease.Linear);
        
        yield return new WaitForSeconds(setTime);
        
        StageManager.Instance.releaseDisplayCube();
        IsMoveGem = false;
        SoundManager.Instance.playSound("Down");
        RotationCube(CurCube.GetComponent<Cube>(), setPickDir);
    }

    int WaitngOnForMovingDirectionCount = 0;
    IEnumerator WaitngOnForMovingCube()
    {
        IsMoveGem = true;

        List<Cube> findlistCube = stackmovingCube.ToList();
        WaitngOnForMovingDirectionCount = 0;
        stackmovingCube.Clear();

        Debug.Log($"ForMovingCube=={findlistCube.Count.ToString()}");
        
        
        foreach (Cube findcube in findlistCube)
        {
            //ksj testCode
            WaitngOnForMovingDirectionCount++;
            StartCoroutine(WaitonMovingCube(findcube));
        }

        yield return new  WaitUntil(()=> WaitngOnForMovingDirectionCount == 0);

        yield return null;
    }

    IEnumerator WaitngOnForExplosionCube()
    {
        IsMoveGem = true;

        List<Cube> findlistCube = listExplosionCube.ToList();

        listExplosionCube.Clear();

        Debug.Log($"ExplosionlistCube=={findlistCube.Count.ToString()}");

        yield return new WaitForSeconds(0.5f);

        foreach (Cube findcube in findlistCube)
        {
            ExplosionCube(findcube);
        }
        yield return null;
    }

    public void   ExplosionCube(Cube cube)
    {
        for (GameDefine.Direction edir = GameDefine.Direction.UP; edir <= GameDefine.Direction.Left; edir++)
        {
            Cube   expCube= getArrayDirMapCube(cube.mX, cube.mY, edir);

            if(expCube != null && expCube.mCode != GameDefine.CubeProperty.None)
            {
                chnageCube(expCube, cube);
                expansionCube(expCube);
            }
               
        }
    }
    IEnumerator WaitngForInfectionMoveCube()
    {
        IsMoveGem = true;

        List<Cube> findlistCube = listInfectionMoveCube.ToList();

        listInfectionMoveCube.Clear();

        Debug.Log($"InfectionMove=={listInfectionMoveCube.Count.ToString()}");

        yield return new WaitForSeconds(0.5f);

        foreach (Cube findcube in findlistCube)
        {
           StartCoroutine(InfectionMoveCube(findcube));
        }
        yield return null;
    }


    IEnumerator InfectionMoveCube(Cube cube)
    {
        Cube NextCube = cube;
        GameDefine.Direction edir = cube.InfectionmovingDir;
        while (true)
        {
            Cube expCube = getArrayDirMapCube(NextCube.mX, NextCube.mY, edir);

            if (expCube == null || expCube.mCode != GameDefine.CubeProperty.Normal)
                break;

            {
                chnageCube(expCube, NextCube);
                yield return new WaitForSeconds(0.1f);
                expansionCube(expCube);
            }
            NextCube = expCube;
        }
        yield return null; 

    }

    IEnumerator WaitonMovingCube(Cube cube)
    {
        int DX = cube.mX, DY = cube.mY;

        bool isTrue = true ;
        cube.mCode = GameDefine.CubeProperty.Normal;
        cube.replace();
        

        while (isTrue)
        {
            //무빙 방향의 반대로 
            switch (cube.movingDir)
            {
                //아래
                case GameDefine.Direction.UP:
                    {
                        GameObject posObject = getPosCube(new Vector2(DX, DY + 1));
                        if (posObject == null || posObject.GetComponent<Cube>().mCode != GameDefine.CubeProperty.Normal)
                            isTrue = false;
                        else
                            DY++;
                    }
                    break;

                    //왼쪽아래
                case GameDefine.Direction.Right:
                    {
                        int addX = 0, addY = 0;
                       if(DX%2 ==0)
                        {
                            addX = -1;
                            addY = +1;
                        }
                        else
                        {
                            addX = -1;
                        }

                        GameObject posObject = getPosCube(new Vector2(DX+addX, DY +addY));

                        if (posObject == null || posObject.GetComponent<Cube>().mCode != GameDefine.CubeProperty.Normal)
                            isTrue = false;
                        else
                        {

                            DX += addX; 
                            DY += addY;
                        }
                    }                   
                    break;
                    //왼쪽
                case GameDefine.Direction.Right_Down:
                    {
                        int addX = 0, addY = 0;
                        if (DX % 2 == 0)
                        {
                            addX = -1;
                        }
                        else
                        {
                            addX = -1;
                            addY = -1;
                        }

                        GameObject posObject = getPosCube(new Vector2(DX + addX, DY + addY));

                        if (posObject == null || posObject.GetComponent<Cube>().mCode != GameDefine.CubeProperty.Normal)
                            isTrue = false;
                        else
                        {

                            DX += addX;
                            DY += addY;
                        }
                    }
                    break;
                    //오른쪽 아래
                case GameDefine.Direction.Left:
                    {
                        int addX = 0, addY = 0;
                        if (DX % 2 == 0)
                        {
                            addX = 1;
                            addY = 1;
                        }
                        else
                        {
                            addX =  1;
                        }

                        GameObject posObject = getPosCube(new Vector2(DX + addX, DY + addY));

                        if (posObject == null || posObject.GetComponent<Cube>().mCode != GameDefine.CubeProperty.Normal)
                            isTrue = false;
                        else
                        {

                            DX += addX;
                            DY += addY;
                        }
                    }
                    break;
                    //오른쪽
                case GameDefine.Direction.Left_Down:
                    {
                        int addX = 0, addY = 0;
                        if (DX % 2 == 0)
                        {
                            addX = 1;
                        }
                        else
                        {
                            addX =  1;
                            addY = -1;
                        }

                        GameObject posObject = getPosCube(new Vector2(DX + addX, DY + addY));

                        if (posObject == null || posObject.GetComponent<Cube>().mCode != GameDefine.CubeProperty.Normal)
                            isTrue = false;
                        else
                        {

                            DX += addX;
                            DY += addY;
                        }
                    }
                    break;
                    //위로
                case GameDefine.Direction.Down:
                    {
                        GameObject posObject = getPosCube(new Vector2(DX, DY - 1));
                        if (posObject == null || posObject.GetComponent<Cube>().mCode != GameDefine.CubeProperty.Normal)
                            isTrue = false;
                        else
                            DY--;
                    }
                    break;
            }

            if(isTrue)
            {
                Cube moveCube = getPosCube(new Vector2(DX, DY)).GetComponent<Cube>();
                moveCube.mCode = GameDefine.CubeProperty.Moving;
                moveCube.replace();

                //애니메이션 플레이관련
                {
                    var anim = moveCube.GetComponent<Animator>();
                    anim.SetTrigger("Action");
                    
                }

                yield return new WaitForSeconds(0.2f);
                moveCube.mCode = GameDefine.CubeProperty.Normal;
                moveCube.replace();
                
            }

        }
        Cube resultCube = getPosCube(new Vector2(DX, DY)).GetComponent<Cube>();

        cube.mCode = GameDefine.CubeProperty.Normal;
        cube.isMoving = false;
        cube.replace();

        resultCube.mCode = GameDefine.CubeProperty.Speical;
        resultCube.replace();
        WaitngOnForMovingDirectionCount--;


        expansionCube(resultCube);
        yield return null;
    }

    
    IEnumerator WaitForCubeFunction()
    {
        IsMoveGem = true;

      while (true)
      {
            List<Cube> findlistCube = listCube.ToList();

            listCube.Clear();

            if (listExplosionCube.Count > 0)
            {
                yield return new WaitForSeconds(0.2f);
                yield return StartCoroutine(WaitngOnForExplosionCube());
            }

            if (listInfectionMoveCube.Count > 0)
            {
                yield return new WaitForSeconds(0.2f);
                yield return StartCoroutine(WaitngForInfectionMoveCube());
            }

            if (stackmovingCube.Count > 0)
            {
                yield return new WaitForSeconds(0.2f);
                yield return StartCoroutine(WaitngOnForMovingCube());
            }

            if (findlistCube.Count > 0)
            {
                yield return new WaitForSeconds(0.2f);

                foreach (Cube findcube in findlistCube)
                {
                    //ksj
                    GameDefine.CubeProperty tCode = findcube.mCode; 
                    findcube.mCode = GameDefine.CubeProperty.Speical;
                    expansionCube(findcube);
                    findcube.mCode = tCode;
                }
              
            }


            if (listCube.Count ==  0 && stackmovingCube.Count == 0  && listExplosionCube.Count ==0)
            {
                IsMoveGem = false;
                  break;
            }
            yield return new WaitForSeconds(0.1f);
        }

        int StageDestinationCount = 0;  
        foreach(var dcub in listDestinationCube)
        {
            if (dcub.mCode != GameDefine.CubeProperty.Speical)
                StageDestinationCount++;
        }

        foreach(var dcube in listMultyiDirectionCube)
        {
            if (dcube.mCode == GameDefine.CubeProperty.MultidirectionalRecognition)
            {
                findMultidirectionalRecognition(dcube);
                dcube.replace();
            }
                
        }
         

        
         GameManager.Instance.ComboCount = 0;

        GameManager.Instance.StageDestinationCount = StageDestinationCount;

        TopView.Instance.UpdateText();

        CheckVertexBox();
        GameClearCheck();

    }

    public void NextStage()
    {
        StageManager.Instance.IsMoveGem = true;
        int NextStep = PlayerManger.Instance.PlayStage+1;

        if (StepManager.Instance.dicStepInfo.ContainsKey(NextStep) == false)
        {
            SceneManager.LoadScene("Title");
            SoundManager.Instance.StopBgm();
            return;
        }
        
        PlayerManger.Instance.PlayStage++;
        
        GameManager.Instance.StageLoad(NextStep);

        StageManager.Instance.IsMoveGem = false;
    }



    public void SpeicalUpdateCube()
    {
        foreach (var dcube in listMultyiDirectionCube)
        {
            if (dcube.mCode == GameDefine.CubeProperty.MultidirectionalRecognition)
            {
                findMultidirectionalRecognition(dcube);
                dcube.replace();
            }

        }
    }

    public void ReStart()
    {
        StageManager.Instance.IsMoveGem = true;
        
        if (StepManager.Instance.dicStepInfo.ContainsKey(PlayerManger.Instance.PlayStage) == false)
            return;

        GameManager.Instance.StageLoad(PlayerManger.Instance.PlayStage);

        StageManager.Instance.IsMoveGem = false;

    }

   
    public void GameClearCheck()
    {
        Debug.Log($"GameClearCheck ==  {GameManager.Instance.TurnLimit.ToString()}");

        if (GameManager.Instance.TurnLimit < 0)
        {
            TopView.Instance.GameOver();
            GameManager.Instance.SaveStageClearinfo(false);
            return;

        }

        if (GameManager.Instance.GameClearCheck())
        {
            TopView.Instance.GameClear();
            GameManager.Instance.SaveStageClearinfo(true);
            //ClearView.SetActive(true);            
        }
        else
        {
            if (GameManager.Instance.TurnLimit == 0)
            {
                TopView.Instance.GameOver();
                GameManager.Instance.SaveStageClearinfo(false);
                return;
            }
        }
    }


    //세균전 관련 
    public bool CheckBlockingWall(Cube mainCube, Cube subCube)
    {
        //block 문제 bug stage 6
        //if (mainCube.mCode == GameDefine.CubeProperty.Speical)
        //    return  false;

        int DirX = mainCube.mX - subCube.mX,
            DirY = mainCube.mY - subCube.mY;

        GameDefine.Direction eDir = GameDefine.Direction.None;

        if (DirX == 0)
        {
            if (DirY == -1)
                eDir = GameDefine.Direction.Down;

            else if (DirY == 0)
                return false;
            else
                eDir = GameDefine.Direction.UP;
        }
        else
        {
            if (mainCube.mX % 2 == 0)
            {
                //Right
                if (DirX == -1)
                {
                    if (DirY == -1)
                        eDir = GameDefine.Direction.Right_Down;
                    else if (DirY == 0)
                        eDir = GameDefine.Direction.Right;
                }
                else
                {
                    if (DirY == -1)
                        eDir = GameDefine.Direction.Left_Down;
                    else if (DirY == 0)
                        eDir = GameDefine.Direction.Left;
                }
            }
            else
            {
                //Right
                if (DirX == -1)
                {
                    if (DirY == 0)
                        eDir = GameDefine.Direction.Right_Down;
                    else if (DirY == 1)
                        eDir = GameDefine.Direction.Right;
                }
                else
                {
                    if (DirY == 0)
                        eDir = GameDefine.Direction.Left_Down;
                    else if (DirY == 1)
                        eDir = GameDefine.Direction.Left;
                }
            }

        }

        if (mainCube.Screens[(int)eDir] == true)
            return true;

          mainCube.movingDir = eDir;

        return false;
    }

    public bool findMultidirectionalRecognition(Cube cube)
    {
        cube.AroundSpeicalCount = 0;
        for (GameDefine.Direction dir= GameDefine.Direction.UP; dir<= GameDefine.Direction.Left; dir++)
        {
            var dirCube = getArrayDirMapCube(cube.mX, cube.mY, dir);

            if (dirCube != null && dirCube.mCode == GameDefine.CubeProperty.Speical)
                cube.AroundSpeicalCount++;            
        }

        if (cube.MultidirectionalRecognitionCount <= cube.AroundSpeicalCount)
            return true;

        return false;

    }

    public bool chnageCube(Cube cube1, Cube cube2)
    {
        if (CheckBlockingWall(cube1, cube2) || CheckBlockingWall(cube2, cube1))
            return false;

        if (cube1.mCode == GameDefine.CubeProperty.Moving)
        {
            cube1.isMoving = true;
            stackmovingCube.Push(cube1);
        }
        else if (cube1.mCode == GameDefine.CubeProperty.Explosion)
        {
            listExplosionCube.Add(cube1);
        }
        else if(cube1.mCode == GameDefine.CubeProperty.MovingInfections)
        {
            listInfectionMoveCube.Add(cube1);
        }
        if (cube1.mCode == GameDefine.CubeProperty.MultidirectionalRecognition)
        {
            if (findMultidirectionalRecognition(cube1) != true)
            {
                cube1.replace();
                return false;
            }

        }
        if (cube1.bIsFixedBlock)
        {
            return false;
        }

        if (cube1.mCode != GameDefine.CubeProperty.None && cube1.mCode != GameDefine.CubeProperty.Normal && cube1.mCode != GameDefine.CubeProperty.Speical)
        {
            GameManager.Instance.StageEnemyCount--;
        }

        if (cube1.mCode == cube2.mCode)
            return false;


        

        GameDefine.CubeProperty prevCode = cube1.mCode;
        cube1.mCode = cube2.mCode;
        SoundManager.Instance.playSound("Change");
        StartCoroutine(infectionDirection(cube1, cube2, prevCode));
        

        return true;
    }

    IEnumerator  infectionDirection(Cube cube1,Cube cube2, GameDefine.CubeProperty prevCode)
    {
        //애니메이션 플레이관련
        {
            var anim = cube1.GetComponent<Animator>();
            anim.SetTrigger("Action");
        }

        yield return  new WaitForSeconds(0.2f);

        GameManager.Instance.ComboCount++;
        TopView.Instance.UpdateText();

        cube1.replace();
        
        if (prevCode == GameDefine.CubeProperty.Explosion)
            StartCoroutine(cube1.PlayBoomEffect());

        if (GameManager.Instance.ComboCount > 1)
            StartCoroutine(cube1.PlayComboEffect(GameManager.Instance.ComboCount));

    }
    public void findChangeCube(GameObject findObject, Cube curCube)
    {
        if (findObject != null)
        {
            Cube findCube = findObject.GetComponent<Cube>();

            if ((findCube.mCode != GameDefine.CubeProperty.Normal && curCube.mCode != GameDefine.CubeProperty.Normal) && (findCube.mCode != curCube.mCode) && (findCube.mCode != GameDefine.CubeProperty.None && curCube.mCode != GameDefine.CubeProperty.None))
            {
                Cube addCube = null;

                if (findCube.mCode == GameDefine.CubeProperty.Speical)
                {
                    if (chnageCube(curCube, findCube))
                    {
                        addCube = curCube;
                    }
                }
                else if (curCube.mCode == GameDefine.CubeProperty.Speical)
                {
                    if (chnageCube(findCube, curCube))
                    {
                        addCube = findCube;
                    }
                }
                else
                    return;

                if (addCube != null)
                {
                    var obj = listCube.Find(x => x.mX == addCube.mX && x.mY == addCube.mY);

                    if (obj == null)
                        listCube.Add(addCube);
                    else
                        Debug.Log("중복");
                }
            }
        }
      }

    // 검색
    void expansionCube(Cube curCube)
    {

        if (curCube.mCode == GameDefine.CubeProperty.None || curCube.mCode == GameDefine.CubeProperty.Normal)
            return; 
        
        //  T               
        findChangeCube(getPosCube(new Vector2(curCube.mX, curCube.mY - 1)), curCube);

        //  R_T
        if (curCube.mX % 2 == 0)
            findChangeCube(getPosCube(new Vector2(curCube.mX+1, curCube.mY)), curCube);
        else
            findChangeCube(getPosCube(new Vector2(curCube.mX+ 1, curCube.mY-1)), curCube);
        
        //  R_B
        if (curCube.mX % 2 == 0)
            findChangeCube(getPosCube(new Vector2(curCube.mX + 1, curCube.mY+1)), curCube);
        else
            findChangeCube(getPosCube(new Vector2(curCube.mX + 1, curCube.mY)), curCube);

        //  B
        findChangeCube(getPosCube(new Vector2(curCube.mX, curCube.mY + 1)), curCube);
        //  L_B
        if (curCube.mX % 2 == 0)
            findChangeCube(getPosCube(new Vector2(curCube.mX - 1, curCube.mY + 1)), curCube);
        else
            findChangeCube(getPosCube(new Vector2(curCube.mX - 1, curCube.mY)), curCube);

        //  L_T
        if (curCube.mX % 2 == 0)
            findChangeCube(getPosCube(new Vector2(curCube.mX - 1, curCube.mY)), curCube);
        else
            findChangeCube(getPosCube(new Vector2(curCube.mX - 1, curCube.mY - 1)), curCube);
    }

}
    