﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioSource myaudio;
    public AudioSource BGMaudio;
    private static SoundManager instance;
    public static SoundManager Instance
    {
        get
        {
            if (instance == null)
            {
                var obj = FindObjectOfType<SoundManager>();

                if (obj != null)
                {
                    instance = obj;
                }
                else
                {
                    var newSingleton = new GameObject("SoundManager").AddComponent<SoundManager>();
                    instance = newSingleton;
                }
            }

            return instance;
        }

        private set
        {
            instance = value;
        }
    }

    private void Awake()
    {
        var objs = FindObjectsOfType<SoundManager>();

        if (objs.Length != 1)
        {

            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(this);
    }

    void Start()
    {

        if(myaudio == null)
        {
            myaudio = gameObject.AddComponent<AudioSource>();
            myaudio.playOnAwake = false;
        }

        if(BGMaudio == null)
        {
            BGMaudio = gameObject.AddComponent<AudioSource>();
            myaudio.playOnAwake = false;
            myaudio.loop = true;
        }
            
    }

    public void  playSound(string name)
    {
        AudioClip playAudio = Resources.Load<AudioClip>($"Sounds\\{name.ToString()}");
        myaudio.PlayOneShot(playAudio);
        
    }
    public void playBgm(string name)
    {
        AudioClip playAudio = Resources.Load<AudioClip>($"Sounds\\{name.ToString()}");
        BGMaudio.clip = playAudio;
        BGMaudio.loop = true;
        BGMaudio.volume = 0.3f;

        BGMaudio.Play();
    }
    public void StopBgm()
    {
        BGMaudio.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
