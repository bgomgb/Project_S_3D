﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottomView : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject adversting;
    private static BottomView instance;
    public static BottomView Instance
    {
        get
        {
            if (instance == null)
            {
                var obj = FindObjectOfType<BottomView>();

                if (obj != null)
                {
                    instance = obj;
                }
                else
                {
                    var newSingleton = new GameObject("BottomView").AddComponent<BottomView>();
                    Instance = newSingleton;
                }
            }

            return instance;
        }

        private set
        {
            instance = value;
        }
    }

    void Start()
    {

    }

    public void updateStage()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
