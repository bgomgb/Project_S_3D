﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.Collections;

public class Cube : MonoBehaviour
{
    public int mX, mY;

    [SerializeField]
    public GameDefine.CubeProperty mCode;

    public bool Destination = false;
    public bool[] Screens = new bool[System.Enum.GetValues(typeof(GameDefine.Direction)).Length-1];

    //스폐셜 큐브 관련 


    public GameDefine.Direction movingDir = GameDefine.Direction.None;
    public bool isMoving = false;

    public int MultidirectionalRecognitionCount = 0 ;
    public int AroundSpeicalCount = 0;
    public TextMesh MDRCounttextMesh;

    public int LinkCube = 0;
    public GameDefine.Direction InfectionmovingDir = GameDefine.Direction.None;
    public SpriteRenderer ImageIcon,DestIcon,LockIcon;

    public bool bIsFixedBlock = false;

    public void RotationCopy(Cube cube)
    {
        this.mCode = cube.mCode;
        this.MultidirectionalRecognitionCount = cube.MultidirectionalRecognitionCount;
    }

    public void replace()
    {
        var meshRender = GetComponent<MeshRenderer>();

        if (meshRender == null)
            Debug.Log("AA");


        if (mCode >= 0)
        {
            this.gameObject.SetActive(true);
            string colorcode = GameDefine.getColor(mCode);
            meshRender.enabled = true;
            Color color;

            if (ColorUtility.TryParseHtmlString(colorcode, out color))
            {
                meshRender.material.color = color;
            }
            else
                Debug.Log("오류");
        }
        else
        {
            this.gameObject.SetActive(false);
        }        

        for(int i =0; i< Screens.Length; i++)
        {
            GameDefine.ScreenWall wall = (GameDefine.ScreenWall)i;

            var obj = this.transform.Find(wall.ToString());
            obj.GetComponent<MeshRenderer>().enabled = Screens[i];

        }

    

        if(MDRCounttextMesh == null)
        {
            MDRCounttextMesh = gameObject.transform.GetComponentInChildren<TextMesh>(); 
        }
        if(mCode == GameDefine.CubeProperty.MultidirectionalRecognition)
        {
            MDRCounttextMesh.text = (MultidirectionalRecognitionCount- AroundSpeicalCount).ToString();
            MDRCounttextMesh.gameObject.SetActive(true);
        }
        else
            MDRCounttextMesh.gameObject.SetActive(false);

        if(DestIcon == null)
        {
            DestIcon = gameObject.transform.Find("Dest_Icon").GetComponent<SpriteRenderer>();
            DestIcon.gameObject.SetActive(false);
        }

        if(DestIcon)
        {
            if (mCode == GameDefine.CubeProperty.Speical)
                DestIcon.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
            else
                DestIcon.gameObject.GetComponent<SpriteRenderer>().color = Color.red;

            DestIcon.gameObject.SetActive(Destination);
        }

        if(LockIcon == null)
        {
            LockIcon = gameObject.transform.Find("Lock_Icon").GetComponent<SpriteRenderer>();
            LockIcon.gameObject.SetActive(false);
        }

        if(LockIcon)
        {
            LockIcon.gameObject.SetActive(bIsFixedBlock);
        }

        if (ImageIcon == null)
        {
            ImageIcon = gameObject.transform.Find("Image_Icon").GetComponent<SpriteRenderer>();
            ImageIcon.gameObject.SetActive(false);
        }
        if(ImageIcon)
        {
            ImageIcon.gameObject.SetActive(false);
            switch (mCode)
            {
                case GameDefine.CubeProperty.Explosion:
                    ImageIcon.gameObject.SetActive(true);
                    ImageIcon.sprite = Resources.Load<Sprite>($"Image\\Boomb");
                    break;
                case GameDefine.CubeProperty.Moving:
                    ImageIcon.gameObject.SetActive(true);
                    ImageIcon.sprite = Resources.Load<Sprite>($"Image\\Move");
                    break;
                case GameDefine.CubeProperty.MovingInfections:
                    ImageIcon.gameObject.SetActive(true);
                    ImageIcon.sprite = Resources.Load<Sprite>($"Image\\Arrow");
                    switch(InfectionmovingDir)
                    {
                        case GameDefine.Direction.UP:
                            ImageIcon.transform.localEulerAngles = new Vector3(90, 90, 0);
                            break;
                        case GameDefine.Direction.Right:
                            ImageIcon.transform.localEulerAngles = new Vector3(90, 145, 0);
                            break;
                        case GameDefine.Direction.Right_Down:
                            ImageIcon.transform.localEulerAngles = new Vector3(90, 210, 0);
                            break;
                        case GameDefine.Direction.Down:
                            ImageIcon.transform.localEulerAngles = new Vector3(90, -90, 0);
                            break;
                        case GameDefine.Direction.Left_Down:
                            ImageIcon.transform.localEulerAngles = new Vector3(90, -30, 0);
                            break;
                        case GameDefine.Direction.Left:
                            ImageIcon.transform.localEulerAngles = new Vector3(90, 30, 0);
                            break;

                    }
                    break;

            }

        }
        var objDestination = this.transform.Find("Image_Icon");

         for (int i = 0; i < objDestination.childCount; i++)
        {
            objDestination.GetChild(i).GetComponent<MeshRenderer>().enabled = false;  
        }

        

    }

     public IEnumerator PlayComboEffect(int Count)
    {
        var EffectObj = this.transform.Find("Effect");

        var comboObj = this.transform.Find("Combo");

        comboObj.gameObject.SetActive(true);
        comboObj.GetComponentInChildren<TextMesh>().text = $"x{Count.ToString()}";

        var effect =Instantiate(GameManager.Instance.comboEffect, EffectObj.transform);

        effect.GetComponent<ParticleSystem>().Play();

        SoundManager.Instance.playSound("ComboSound");
        
        yield return new WaitForSeconds(0.3f);
        comboObj.gameObject.SetActive(false);

        yield return new WaitUntil(() => effect.GetComponent<ParticleSystem>().isPlaying == false);
        Destroy(effect);

    }

    public IEnumerator PlayBoomEffect()
    {
        var EffectObj = this.transform.Find("Effect");

        var effect = Instantiate(GameManager.Instance.boomeffect, EffectObj.transform);

        effect.GetComponent<ParticleSystem>().Play();
        SoundManager.Instance.playSound("Boomb");

        yield return new WaitUntil(() => effect.GetComponent<ParticleSystem>().isPlaying == false);

        Destroy(effect);

    }
}