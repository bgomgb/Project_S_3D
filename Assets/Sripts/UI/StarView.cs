﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarView : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject[] ListMainStarObject = new GameObject[3];
    public GameObject[] ListBestStarObject = new GameObject[3];

    public Text BestClearTime, CurrentTime;
    public Text LimitTwoStarTurntext, LimitThreeStarTurnText;
    
    void Start()
    {
      
    }

    public void init()
    {

        var playerStageClearInfo = PlayerManger.Instance.stageClearInfos.Find(x => x.Stage == PlayerManger.Instance.PlayStage);


        for(int i=0; i<3; i++)        
        {
            if(i<playerStageClearInfo.StarRank)
            {
                ListMainStarObject[i].GetComponent<Image>().sprite = Resources.Load<Sprite>($"Image\\Star");
            }
            else 
            {
                ListMainStarObject[i].GetComponent<Image>().sprite = Resources.Load<Sprite>($"Image\\EStar");
            }
        }

        for (int i = 0; i < 3; i++)
        {
            if (i < playerStageClearInfo.BestClearRank)
            {
                ListBestStarObject[i].GetComponent<Image>().sprite = Resources.Load<Sprite>($"Image\\Star");
            }
            else
            {
                ListBestStarObject[i].GetComponent<Image>().sprite = Resources.Load<Sprite>($"Image\\EStar");
            }
        }




        {
            int min = (int)playerStageClearInfo.BestClearTime / 60;
            int sec = (int)((playerStageClearInfo.BestClearTime) - min * 60);

            BestClearTime.text = min.ToString("00") + ":" + sec.ToString("00");

        }

        {
            int min = (int)playerStageClearInfo.ClearTime / 60;
            int sec = (int)((playerStageClearInfo.ClearTime) - min * 60);

            CurrentTime.text = min.ToString("00") + ":" + sec.ToString("00");
        }




        StepInfo obj = GameManager.Instance.GetCurrentStepInfo();
        LimitTwoStarTurntext.text =": "+ obj.Two_StarAttainmentTargetTurn.ToString() + " Turn";
        LimitThreeStarTurnText.text = ": " + obj.Three_StarAttainmentTargetTurn.ToString() + " Turn";
    }

}
