﻿using UnityEngine;
using UnityEditor;

public static class GameDefine 
{
    public enum CubeProperty
    {
        None                         = -1,                 
        Normal                       = 0,
        Speical                      = 1,
        Moving                       = 2,
        Explosion                    = 3,
        Enemy                        = 4,
        MultidirectionalRecognition  = 5,
        MovingInfections             = 6,
        LinkInfection                = 7,
        Occupation                   = 8,
    }
    public enum Direction
    {
        None = -1,
        UP,
        Right,
        Right_Down,
        Down,
        Left_Down,
        Left,
    }
    public enum ScreenWall 
    {
        T_W = 0,
        R_W,
        RD_W,
        B_W,
        LD_W,
        L_W,
    }

    public enum VictoryConditions
    {
        All         = 0,
        Destination = 1, 
        ALL_DEST    = 2, 
    }

    public static string getColor(GameDefine.CubeProperty code)
    {
        string colorcode = "FFFFFF";

        switch (code)
        {
            case GameDefine.CubeProperty.None:   //흰
                colorcode = "#FFFFFF";
                break;
            case GameDefine.CubeProperty.Normal:    // 빨
                colorcode = "#FFFFFF";
                break;
            case GameDefine.CubeProperty.Enemy:   // 살
                colorcode = "#0000FF"; //#fbceb1
                break;
            case GameDefine.CubeProperty.Moving:   // 노
                colorcode = "#00bb00";
                break;
            case GameDefine.CubeProperty.Speical:  //  파
                colorcode = "#ff0000";
                break;
            case GameDefine.CubeProperty.Explosion:  //  
                colorcode = "#008000";
                break;

            case GameDefine.CubeProperty.MultidirectionalRecognition:
                colorcode = "#00bbFF";
                break;

            case GameDefine.CubeProperty.MovingInfections:
                colorcode = "#FFbb00";
                break;

            case GameDefine.CubeProperty.LinkInfection:
                colorcode = "#CC3300";
                break;
            case GameDefine.CubeProperty.Occupation:
                colorcode = "#CCaa00";
                break;

        }
        return colorcode;
    }

}