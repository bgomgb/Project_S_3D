﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerManger))]
public class JsonEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("SaveStageClearInfo"))
        {
            PlayerManger.Instance.Save();
        }
        if (GUILayout.Button("LoadStageClearInfo"))
        {
            PlayerManger.Instance.Load();
        }
    }
}