﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageSelect : MonoBehaviour
{
    // Start is called before the first frame update
    public List<StageButton> btnList;
    public Button BackButton;
    public Text pageText;
    public int page;
    int EndPage;
    public GameObject StageSelectView;


    public Button btnLeftPage, btnRightPage, btnFirtPage, btnEndPlage;
    void Start()
    {
        btnLeftPage.onClick.AddListener(() => LeftStage());
        btnRightPage.onClick.AddListener(() => RightStage());
        btnFirtPage.onClick.AddListener(() => FirstStage());
        btnEndPlage.onClick.AddListener(() => EndStage());

        BackButton.onClick.AddListener(() => BackOFF());

        foreach (var btn in btnList)
        {
            btn.StageSeletView = gameObject;
        }
    }

    public void LeftStage()
    {
        if(page>1)
        {
            page--;
            SetBtnList(page);
        }
    }
    public void RightStage()
    {
        if (page < EndPage)
        {
            page++;
            SetBtnList(page);
        }
    }
    public void FirstStage()
    {
        page = 1;
        SetBtnList(page);
    }
    public void EndStage()
    {
        page = EndPage;
        SetBtnList(page);
    }
    public void  StageSelectOn()
    {
        EndPage = 1+ PlayerManger.Instance.EndGameStage / btnList.Count;
        SetBtnList(1);
        gameObject.SetActive(true);
        pageText.text = page.ToString();
    }
    public void SetBtnList(int setpage)
    {
        page = setpage;
        int index = 1 + (page-1)* btnList.Count;
        foreach (var btn in btnList)
        {
            if(index <= PlayerManger.Instance.EndGameStage)
            {

                if (PlayerManger.Instance.ClearStage+1 >= index)
                    btn.btnSet(index, false);
                else
                    btn.btnSet(index, true);                
            }
            else
            {
                

                    btn.btnSet(0, true);
            }
            
            index++;
        }
        pageText.text = page.ToString();
    }
    void BackOFF()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
