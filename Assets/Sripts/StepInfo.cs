﻿using UnityEngine;
using UnityEditor;
using System;

[Serializable]
public class StepInfo
{
    public int Step;
    public string FileName;

    public float CameraInfoX;
    public float CameraInfoY;
    public float CameraInfoZ;
    public float OthSize;
    
    public float RotationX;
    public float RotationY;
    public float RotationZ;

    public int Victoryconditions;
    public int TurnLimit;
    public int ComboCount;

    public int Two_StarAttainmentTargetTurn;
    public int Three_StarAttainmentTargetTurn;

    public float HexagonFieldX;
    public float HexagonFieldY;
    public float HexagonFieldZ;
    
    public float HexagonField_RotationX;
    public float HexagonField_RotationY;
    public float HexagonField_RotationZ;

}
[Serializable]
public class StepInfoDictionary : SerializableDictionary<int, StepInfo> { }