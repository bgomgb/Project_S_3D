﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TutorialManager : MonoBehaviour
{
    // Start is called before the first frame update
    private string strFileName = "Csv/Tutorial";
    public GameObject TutorialView;
    public Image mainImage;
    public Text subText;
    public Button nextbtn;
    public int Page,TotalPage;
    public int ViewStage = 0;
    public class TutorialInfo{
        public int index;
        public string text;
        public string imagePath;
    }
    public Dictionary<int, List<TutorialInfo>> dicTutoinfo = new Dictionary<int, List<TutorialInfo>>();

    void Start()
    {
        nextbtn.onClick.AddListener(() => Next());
        TutorialView.SetActive(false);
        TopView.Instance.isPlayTime = false;
        Load();
        init();
    }

    public void Next()
    {

        Page++;

        if (dicTutoinfo[PlayerManger.Instance.PlayStage].Count == Page)
        {
            TutorialView.SetActive(false);
            Page = 0;
            TopView.Instance.isPlayTime = true;
        }
        else 
        {
            string ImagePath = "Image\\Tutorial\\" + $"{PlayerManger.Instance.PlayStage.ToString()}\\{dicTutoinfo[PlayerManger.Instance.PlayStage][Page].imagePath.ToString()}";
            mainImage.sprite = Resources.Load<Sprite>(ImagePath.ToString());
            subText.text = dicTutoinfo[PlayerManger.Instance.PlayStage][Page].text;
        }
    }

    public void Load()
    {
        List<Dictionary<string, object>> data = CsvReader.Read(strFileName);

        if (dicTutoinfo.Count != 0)
        {
            dicTutoinfo.Clear();
        }

        for (var i = 0; i < data.Count; i++)
        {
            TutorialInfo readObj = new TutorialInfo();

            int Stage = int.Parse(data[i]["stage"].ToString());

            if(dicTutoinfo.ContainsKey(Stage)==false)
            {
                dicTutoinfo.Add(Stage, new List<TutorialInfo>());
            }

            readObj.index       = int.Parse(data[i]["Index"].ToString());
            readObj.text      = data[i]["Text"].ToString();
            readObj.imagePath = data[i]["Image"].ToString();
            dicTutoinfo[Stage].Add(readObj);
        }
    }

    public void init()
    {

        //if (PlayerManger.Instance.PlayStage > PlayerManger.Instance.ClearStage)
        //    return;

        if (dicTutoinfo.ContainsKey(PlayerManger.Instance.PlayStage) == false)
        {
            TutorialView.SetActive(false);
            TopView.Instance.isPlayTime = true;
            return;
        }

        if (PlayerManger.Instance.PlayStage > 3)
        {
            TopView.Instance.isPlayTime = true;
            return;
        }
        if (ViewStage == PlayerManger.Instance.PlayStage)
        {
            TopView.Instance.isPlayTime = true;
            return;
        }

        
        TutorialView.SetActive(true);
        string ImagePath = "Image\\Tutorial\\"+$"{PlayerManger.Instance.PlayStage.ToString()}\\{dicTutoinfo[PlayerManger.Instance.PlayStage][Page].imagePath.ToString()}";
        mainImage.sprite = Resources.Load<Sprite>(ImagePath.ToString());
        subText.text = dicTutoinfo[PlayerManger.Instance.PlayStage][Page].text;

    }
}
