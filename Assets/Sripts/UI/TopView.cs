﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TopView : MonoBehaviour
{
    // Start is called before the first frame update
    public Text textTurnLimit, textCombo,textPlaytime,textStage;
    public GameObject Goal, Goals;
    public Button btnReset, btnNext, btnHome;
    private static TopView instance;
    public GameObject VidoeView,StarView;
    public GameObject FailView,ClearView;
    public bool isPlayTime = false;
    public float playtime;  
    
    
    public static TopView Instance
    {
        get
        {
            if (instance == null)
            {
                var obj = FindObjectOfType<TopView>();

                if (obj != null)
                {
                    instance = obj;
                }
                else
                {
                    var newSingleton = new GameObject("TopView").AddComponent<TopView>();
                    Instance = newSingleton;
                }
            }

            return instance;
        }

        private set
        {
            instance = value;
        }
    }


    void Start()
    {
        
        btnReset.onClick.AddListener(() => ResetStage());
        btnNext.onClick.AddListener(() => NextStage());
        btnHome.onClick.AddListener(() => BackHome());
        playtime = 0.0f;
    }

    public void ResetStage()
    {
        //   StopAllCoroutines();
        if (StageManager.Instance.IsMoveGem == true)
            return; 

        StageManager.Instance.ReStart();
       GoogleAdsManager.Instance.ShowInterstitialAd(false);

    }
    public void NextStage()
    {
        if (StageManager.Instance.IsMoveGem == true)
            return;

        StageManager.Instance.NextStage();
    }
    public void BackHome()
    {
        if (StageManager.Instance.IsMoveGem == true)
            return;

        SceneManager.LoadScene("Title");
        GoogleAdsManager.Instance.HideBannerAd();
        SoundManager.Instance.StopBgm();
    }


    public void init()
    {
        //클리어 목표
        for (int i = 0; i < Goals.transform.childCount; i++)
        {
//#if UNITY_EDITOR
//            DestroyImmediate(Goals.transform.GetChild(i).gameObject);
//#else
                  Destroy(Goals.transform.GetChild(i).gameObject);
//#endif

        }


        switch (GameManager.Instance.victoryConditions)
        {
            case GameDefine.VictoryConditions.All:
                {
                    
                    var obj = Instantiate(Goal, Goals.transform);
                    obj.transform.name = "Enemy";
                    obj.transform.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("Image\\Hexagon");
                    obj.transform.GetComponentInChildren<Image>().SetNativeSize();
                    obj.transform.GetComponentInChildren<Text>().text = $"x{GameManager.Instance.StageEnemyCount.ToString()}";

                    break;
                }

            case GameDefine.VictoryConditions.Destination:
                {
                    var obj = Instantiate(Goal, Goals.transform);
                    obj.transform.name = "Dest";
                    obj.transform.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("Image\\Destination");
                    obj.transform.GetComponentInChildren<Image>().SetNativeSize();
                    obj.transform.GetComponentInChildren<Text>().text = $"x{GameManager.Instance.StageDestinationCount.ToString()}";
                    
                    break;
                }


            case GameDefine.VictoryConditions.ALL_DEST:
                {
                    var obj = Instantiate(Goal, Goals.transform);
                    obj.transform.name = "Enemy";
                    obj.transform.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("Image\\Hexagon");
                    obj.transform.GetComponentInChildren<Image>().SetNativeSize();
                    obj.transform.GetComponentInChildren<Text>().text = $"x{GameManager.Instance.StageEnemyCount.ToString()}";

                    var obj1 = Instantiate(Goal, Goals.transform);
                    obj1.transform.name = "Dest";
                    obj1.transform.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("Image\\Destination");
                    obj1.transform.GetComponentInChildren<Image>().SetNativeSize();
                    obj1.transform.GetComponentInChildren<Text>().text = $"x{GameManager.Instance.StageDestinationCount.ToString()}";
                    break;
                }
        }

        if(GameManager.Instance.StageMaxComboCount > 0)
        {
            var obj = Instantiate(Goal, Goals.transform);
            obj.transform.name = "Combo";
            obj.transform.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("Image\\Combo");
            obj.transform.GetComponentInChildren<Image>().SetNativeSize();
            obj.transform.GetComponentInChildren<Text>().text = $"x{GameManager.Instance.StageMaxComboCount.ToString()}";
        }

        textStage.text = $"STAGE {PlayerManger.Instance.PlayStage.ToString()} ";
        textTurnLimit.text = $"x{GameManager.Instance.TurnLimit.ToString()}";
        textCombo.text = $"x{GameManager.Instance.ComboCount.ToString()}";
        isPlayTime = true;
        playtime = 0.0f;
        StarView.gameObject.SetActive(false);

        //Tutorial.GetComponent<TutorialManager>().init();

        VidoeView.GetComponent<VideoView>().Init();
    }

    // Update is called once per frame
    public void UpdateText()
    {

        var Enemy = Goals.transform.Find("Enemy");
        if(Enemy != null)
            Enemy.transform.GetComponentInChildren<Text>().text = $"x{GameManager.Instance.StageEnemyCount.ToString()}";

        var Dest = Goals.transform.Find("Dest");
        if (Dest != null)
            Dest.transform.GetComponentInChildren<Text>().text = $"x{GameManager.Instance.StageDestinationCount.ToString()}";        

        textTurnLimit.text = $"x{GameManager.Instance.TurnLimit.ToString()}";

        GameManager.Instance.MaxComboCount = Mathf.Max(GameManager.Instance.MaxComboCount, GameManager.Instance.ComboCount);
        textCombo.text = $"x{GameManager.Instance.MaxComboCount.ToString()}";
    }

    IEnumerator WaitonView(GameObject obj)

    {
        yield return new WaitForSeconds(0.4f);
        obj.SetActive(true);
        StarView.GetComponent<StarView>().init();
        StarView.gameObject.SetActive(true);
        
    }
    public void GameClear()
    {
        PlayerManger.Instance.ClearStage = Mathf.Max(PlayerManger.Instance.ClearStage, PlayerManger.Instance.PlayStage);
        GameClearFailViewOn(ClearView);      
       
    }
    public void GameOver()
    {
        GameClearFailViewOn(FailView);
    }

    public void Update()
    {
        if (isPlayTime)
        {
            playtime += Time.deltaTime;
        }

        int min = (int)playtime / 60;
        int sec = (int)((playtime) - min * 60);

        textPlaytime.text =  min.ToString("00") + ":"+sec.ToString("00");
    }

    
    void GameClearFailViewOn(GameObject obj)
    {
        StageManager.Instance.IsMoveGem = true;
       GoogleAdsManager.Instance.HideBannerAd();

        StartCoroutine(WaitonView(obj));
    }

}
