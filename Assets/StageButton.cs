﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StageButton : MonoBehaviour
{
    // Start is called before the first frame update
    public Button btnClick;
    public Image Lock;
    public Text Number,Time;
    public GameObject StageSeletView;
    public GameObject[] StarRank= new GameObject[3];
    void Start()
    {
        btnClick.onClick.AddListener(() => StageLoad());
    }
    public void btnSet(int index,bool isLock)
    {
        if (isLock)
            Lock.gameObject.SetActive(true);
        else
            Lock.gameObject.SetActive(false);

        Number.text = index.ToString();

        var Data = PlayerManger.Instance.stageClearInfos.Find(x => x.Stage == index);

        if(Data == null)
        {
            for (int i = 0; i < 3; i++)
                StarRank[i].gameObject.SetActive(false);

        }
        else
        {
             for (int i = 0; i < 3; i++)
            {
                StarRank[i].gameObject.SetActive(true);

                if (i < Data.BestClearRank)
                {
                    StarRank[i].GetComponent<Image>().sprite = Resources.Load<Sprite>($"Image\\Star");
                }
                else
                {
                    StarRank[i].GetComponent<Image>().sprite = Resources.Load<Sprite>($"Image\\EStar");
                }
            }

            int min = (int)Data.BestClearTime / 60;
            int sec = (int)((Data.BestClearTime) - min * 60);

            Time.text = min.ToString("00") + ":" + sec.ToString("00");
        }


    }
    void StageLoad()
    {
        if (Lock.IsActive())
            return;

        int stage = int.Parse(Number.text.ToString());

        PlayerManger.Instance.PlayStage = stage;

        if (SceneManager.GetActiveScene().name == "Title")
        {
            SceneManager.LoadScene("Main");
            SoundManager.Instance.playBgm("InGameBGM_Bubble");
        }
        else
            GameManager.Instance.StageLoad(PlayerManger.Instance.PlayStage);

        StageSeletView.gameObject.SetActive(false);

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
