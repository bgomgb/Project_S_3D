﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public  class MapFileManager : MonoBehaviour
{
    private GameObject cube;
    public  void Save()
    {

        if (StageManager.Instance.stageName == null)
        {
            Debug.Log("Save 오류");
            return;
        }

        using (var writer = new CsvFileWriter($"Assets/Resources/Csv/{StageManager.Instance.stageName}.csv"))
        {
            Debug.Log("Save ");
       
            List<string> columns = new List<string>() { "X", "Y", "Code", "Width", "Height", "Destination", "UP_Wall", "Right_Wall", "RD_Wall", "DownWall", "LD_Wall","Left_Wall","ETC1", "ETC2", "ETC3","FixedBlock" };// making Index Row
            writer.WriteRow(columns);
            columns.Clear();
            columns.Add("0"); // Name
            columns.Add("0"); // Level
            columns.Add("0"); // Hp
            columns.Add(StageManager.Instance.mWidth.ToString()); 
            columns.Add(StageManager.Instance.mHeight.ToString());

            writer.WriteRow(columns);
            columns.Clear();

            cube = GameObject.Find("Hexagon_Field");

            for (int i = 0; i < StageManager.Instance.mWidth; i++)
            {
                for (int j = 0; j < StageManager.Instance.mHeight; j++)
                {
                    Cube obj = cube.transform.Find($"{i.ToString()}_{j.ToString()}").GetComponent<Cube>();
                    //Cube obj = arrayMap[i, j].GetComponent<Cube>();
                    columns.Add(obj.mX.ToString()); // Name
                    columns.Add(obj.mY.ToString()); // Level
                    int code = (int)obj.mCode;

                    columns.Add(code.ToString()); 
                    columns.Add("0"); 
                    columns.Add("0");
                    //"Destination", 

                    
                    columns.Add(obj.Destination.ToString());
                    //"UP_Wall" 
                    columns.Add(obj.Screens[0].ToString());
                    // "Right_Wall"
                    columns.Add(obj.Screens[1].ToString());
                    // "RD_Wall"
                    columns.Add(obj.Screens[2].ToString());
                    //"DownWall"
                    columns.Add(obj.Screens[3].ToString());
                    //"LD_Wall"
                    columns.Add(obj.Screens[4].ToString());
                    //"Left_Wall"
                    columns.Add(obj.Screens[5].ToString());

                    //ETC1
                    if(obj.mCode == GameDefine.CubeProperty.MultidirectionalRecognition)
                      columns.Add(obj.MultidirectionalRecognitionCount.ToString());
                    else if (obj.mCode == GameDefine.CubeProperty.MovingInfections)
                        columns.Add(((int)(obj.InfectionmovingDir)).ToString());
                    else
                        columns.Add("0");
                    //ETC2
                        columns.Add("0");
                    //ETC3
                        columns.Add("0");

                    columns.Add(obj.bIsFixedBlock.ToString());

                    writer.WriteRow(columns);

                    columns.Clear();
                }
            }

        }
    }

    public  void Load(string stage)
    {

        List<Dictionary<string, object>> data = CsvReader.Read("Csv/"+stage);

        StageManager.Instance.mWidth = int.Parse(data[0]["Width"].ToString());
        StageManager.Instance.mHeight = int.Parse(data[0]["Height"].ToString());

        cube = GameObject.Find("Hexagon_Field");

        if (cube == null)
        {
            Debug.Log("Gems 오브젝트를 찾을수 없다");
            return;
        }

        if (cube.transform.childCount != 0)
        {
            for (int i = cube.transform.childCount - 1; i >= 0; i--)
                DestroyImmediate(cube.transform.GetChild(i).gameObject);
        }

        StageManager.Instance.stageName = stage;

        StageManager.Instance.arrayMap = new GameObject[StageManager.Instance.mWidth, StageManager.Instance.mHeight];

        float top = cube.transform.position.y;

        float gap = 0.25f;
        //int iy = (int)(1080 - (mWidth - 1) * gap);
        float blockWidth = 5.9f;
        float blockHight = 5.15f;

        float fixX = 0;// blockWidth/2;
        float fixY = 0;// blockHight/2;


        for (var i = 0; i < StageManager.Instance.mWidth; i++)
        {
            if (i != 0)
                fixX += blockWidth / 2 + (blockWidth / 4) + gap;
            if (i % 2 == 0)
                fixY = blockHight;
            else
                fixY = blockHight / 2;
            for (var j = 0; j < StageManager.Instance.mHeight; j++)
            {

                GameObject gReadObject=new GameObject();
                var readObj = gReadObject.AddComponent<Cube>();

                readObj.mCode = (GameDefine.CubeProperty)int.Parse(data[1 + (i * StageManager.Instance.mHeight) + j]["Code"].ToString());

                if (readObj.mCode != GameDefine.CubeProperty.None && readObj.mCode != GameDefine.CubeProperty.Normal && readObj.mCode != GameDefine.CubeProperty.Speical)
                    GameManager.Instance.StageEnemyCount++;                
                
                readObj.Destination = bool.Parse(data[1 + (i * StageManager.Instance.mHeight) + j]["Destination"].ToString());
                readObj.Screens[0]= bool.Parse(data[1 + (i * StageManager.Instance.mHeight) + j]["UP_Wall"].ToString());
                readObj.Screens[1] = bool.Parse(data[1 + (i * StageManager.Instance.mHeight) + j]["Right_Wall"].ToString());
                readObj.Screens[2] = bool.Parse(data[1 + (i * StageManager.Instance.mHeight) + j]["RD_Wall"].ToString());
                readObj.Screens[3] = bool.Parse(data[1 + (i * StageManager.Instance.mHeight) + j]["DownWall"].ToString());
                readObj.Screens[4] = bool.Parse(data[1 + (i * StageManager.Instance.mHeight) + j]["LD_Wall"].ToString());
                readObj.Screens[5] = bool.Parse(data[1 + (i * StageManager.Instance.mHeight) + j]["Left_Wall"].ToString());

                if((GameDefine.CubeProperty)int.Parse(data[1 + (i * StageManager.Instance.mHeight) + j]["Code"].ToString()) == GameDefine.CubeProperty.MultidirectionalRecognition)
                {
                    Debug.Log("다방향테스트");
                    readObj.MultidirectionalRecognitionCount = int.Parse(data[1 + (i * StageManager.Instance.mHeight) + j]["ETC1"].ToString());
                    
                }

                if ((GameDefine.CubeProperty)int.Parse(data[1 + (i * StageManager.Instance.mHeight) + j]["Code"].ToString()) == GameDefine.CubeProperty.MovingInfections)
                {
                    Debug.Log("방향성 전염");
                    readObj.InfectionmovingDir = (GameDefine.Direction)(int.Parse(data[1 + (i * StageManager.Instance.mHeight) + j]["ETC1"].ToString()));
                }

                if(data[1 + (i * StageManager.Instance.mHeight) + j].ContainsKey("FixedBlock"))
                    readObj.bIsFixedBlock = bool.Parse(data[1 + (i * StageManager.Instance.mHeight) + j]["FixedBlock"].ToString());

                StageManager.Instance.CreateObjcet(i, j, fixX, fixY + ((blockHight + gap) * j), blockWidth, blockHight, readObj);
#if UNITY_EDITOR
                DestroyImmediate(gReadObject);
#else
                Destroy(gReadObject);
#endif



            }
        }

        StageManager.Instance.CheckVertexBox();
        StageManager.Instance.IsMoveGem = false;
        //BottomView.Instance.updateStage();
    }

}